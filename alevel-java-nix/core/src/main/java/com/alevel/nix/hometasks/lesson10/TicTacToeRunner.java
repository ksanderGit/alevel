package com.alevel.nix.hometasks.lesson10;

import com.alevel.nix.hometasks.lesson10.controller.TicTacToeGame;
import com.alevel.nix.hometasks.lesson10.model.TicTacToe3x3;

public class TicTacToeRunner {
    public static void main(String[] args) {
        TicTacToeGame ticTacToeGame = new TicTacToeGame(new TicTacToe3x3());
        ticTacToeGame.loop();
    }
}
