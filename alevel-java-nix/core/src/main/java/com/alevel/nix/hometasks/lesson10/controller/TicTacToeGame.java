package com.alevel.nix.hometasks.lesson10.controller;

import com.alevel.nix.hometasks.lesson10.model.TicTacToe;
import com.alevel.nix.hometasks.lesson10.view.TurnResultView;

public class TicTacToeGame {
    private boolean flag;
    private boolean isCurrentX = false;
    private int n;
    private TicTacToe model;
    private TurnResultView resultView;

    public TicTacToeGame(TicTacToe model) {
        this.model = model;
    }

    public void loop(){
        do {
            isCurrentX = !isCurrentX;
            resultView = model.createCanvas();
            System.out.println("input number for mark " + (isCurrentX ? "X" : "O"));
            n = model.getNumber();
            if (n == -1) break;
            resultView.log.info("Player" + (isCurrentX ? "X" : "O") + " input number " + n);
            try {
                resultView.view[n] = isCurrentX ? 1 : 2;
            }catch (ArrayIndexOutOfBoundsException e) {
                resultView.log.error(e.getMessage());
            }
            flag = !model.mark(n);
        } while (flag);
        model.createCanvas();
        resultView.print(n, isCurrentX);
    }

}
