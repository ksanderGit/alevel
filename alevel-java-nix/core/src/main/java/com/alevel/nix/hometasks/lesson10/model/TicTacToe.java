package com.alevel.nix.hometasks.lesson10.model;

import com.alevel.nix.hometasks.lesson10.view.TurnResultView;

public interface TicTacToe {
    boolean mark(int n);
    int getNumber();
    TurnResultView createCanvas();
}
