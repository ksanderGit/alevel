package com.alevel.nix.hometasks.lesson10.model;

import com.alevel.nix.hometasks.lesson10.view.TurnResultView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TicTacToe3x3 implements TicTacToe {
    private static final int SIDE = 3;
    public int[] canvas = new int[SIDE * SIDE];
    private int counter = canvas.length;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static final Logger log = LoggerFactory.getLogger(TicTacToe3x3.class);

    @Override
    public boolean mark(int n) {
        if (n < 0 || n > canvas.length) {
            return false;
        }
        int row = n - n % 3;
        if (canvas[row] == canvas[row + 1] &&
                canvas[row] == canvas[row + 2]) return true;

        int column = n % 3;
        if (canvas[column] == canvas[column + 3])
            if (canvas[column] == canvas[column + 6]) return true;

        if (n % 2 != 0) return false;

        if (n % 4 == 0) {
            if (canvas[0] == canvas[4] &&
                    canvas[0] == canvas[8]) return true;
            if (n != 4) return false;
        }
        return canvas[2] == canvas[4] &&
                canvas[2] == canvas[6];
    }

    @Override
    public int getNumber() {
        while (counter != 0) {
            try {
                int n = Integer.parseInt(reader.readLine());
                if (n >= 0 && n < canvas.length && canvas[n]==0) {
                    counter--;
                    return n;
                }
                if (n > canvas.length || n < 0) {
                    return n;
                }
                log.info("Choose free cell and enter its number");
            } catch (NumberFormatException e) {
                log.debug("Please enter the number");
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return -1;
    }
    public TurnResultView createCanvas() {
        System.out.println("     |     |     ");
        for (int i = 0; i < canvas.length; i++) {
            if (i!=0){
                if (i%3==0) {
                    System.out.println();
                    System.out.println("_____|_____|_____");
                    System.out.println("     |     |     ");
                }
                else
                    System.out.print("|");
            }

            if (canvas[i]==0) System.out.print("  " + i + "  ");
            if (canvas[i]==1) System.out.print("  X  ");
            if (canvas[i]==2) System.out.print("  O  ");
        }
        System.out.println();
        System.out.println("     |     |   ");
        return new TurnResultView(canvas, log);
    }
}
