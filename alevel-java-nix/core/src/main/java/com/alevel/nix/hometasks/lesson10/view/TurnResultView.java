package com.alevel.nix.hometasks.lesson10.view;

import org.slf4j.Logger;

public class TurnResultView {
    public int[] view;
    public Logger log;

    public TurnResultView(int[] view, Logger log) {
        this.view = view;
        this.log = log;
    }

    public void print(int n, boolean isCurrentX) {
        if (n == -1) {
            log.info("Draw");
            System.out.println("Draw");
        }else {
            log.info("The winner is Player" + (isCurrentX ? "X" : "O") + "!");
            System.out.println("The winner is Player" + (isCurrentX ? "X" : "O") + "!");
        }
    }

}
