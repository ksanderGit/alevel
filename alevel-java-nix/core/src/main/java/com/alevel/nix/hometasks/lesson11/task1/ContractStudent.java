package com.alevel.nix.hometasks.lesson11.task1;

import java.util.Objects;

public class ContractStudent extends Student {
    double contractPrice;

    public ContractStudent(String name, int age, double contractPrice) {
        super(name, age);
        this.contractPrice = contractPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractStudent that = (ContractStudent) o;
        return Double.compare(that.contractPrice, contractPrice) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contractPrice);
    }

    @Override
    public String toString() {
        return "ContractStudent name: " + name +
                "   contractPrice = " + contractPrice;
    }
}
