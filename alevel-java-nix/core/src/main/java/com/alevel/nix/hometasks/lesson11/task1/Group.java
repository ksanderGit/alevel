package com.alevel.nix.hometasks.lesson11.task1;

import java.util.ArrayList;
import java.util.List;

public class Group {
   List<Student> getContractStudents(Student[] students) {
       List<Student> contractStudents = new ArrayList<>();
        for (Student student : students) {
            if (student instanceof ContractStudent) {
               contractStudents.add(student);
                System.out.println(student);
            }
        }
        return contractStudents;
    }
}
