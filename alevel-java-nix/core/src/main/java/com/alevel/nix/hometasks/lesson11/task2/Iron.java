package com.alevel.nix.hometasks.lesson11.task2;

public class Iron implements Substance {
    private double temperature = TEMPERATURE;

    @Override
    public State heatUp(double t) {
        temperature += t;
        if (temperature > 1538 && temperature < 2900) {
            return State.LIQUID;
        }else if (temperature > 2900){
            return State.GAS;
        }else {
            return State.SOLID;
        }
    }

    @Override
    public double getTemperature() {
        return temperature;
    }
}
