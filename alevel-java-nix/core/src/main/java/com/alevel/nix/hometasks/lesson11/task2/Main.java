package com.alevel.nix.hometasks.lesson11.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Substance substance = null;
        System.out.println("Select desired substance \n" +
                "Water \n" +
                "Oxygen \n" +
                "Iron");
        while (substance == null) {
            switch (reader.readLine()) {
                case "Water":
                    substance = new Water();
                    break;
                case "Oxygen":
                    substance = new Oxygen();
                    break;
                case "Iron":
                    substance = new Iron();
                    break;
            }
            if (substance == null)
            System.out.println("Select substance from list");
        }
            while (true) {
                System.out.println("Enter temperature");
                double temperature;
                try {
                    temperature = Double.parseDouble(reader.readLine());
                }catch (NumberFormatException e) {
                    System.out.println("Enter only numbers");
                    continue;
                }
                if (temperature == 0) {
                    break;
                }
                System.out.printf("State substance %s temperature substance %.2f℃ \n", substance.heatUp(temperature), substance.getTemperature());
            }
    }
}
