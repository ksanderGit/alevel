package com.alevel.nix.hometasks.lesson11.task2;

public class Oxygen implements Substance {
    private double temperature = TEMPERATURE;
    @Override
    public State heatUp(double t) {
        temperature += t;
        if (temperature > -182) {
            return State.GAS;
        }else if (temperature > -220 && temperature <= -183) {
            return State.LIQUID;
        }else {
            return State.SOLID;
        }
    }

    @Override
    public double getTemperature() {
        return temperature;
    }
}
