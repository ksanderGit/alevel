package com.alevel.nix.hometasks.lesson11.task2;

public interface Substance {
    double TEMPERATURE = 20.0;
    State heatUp(double t);
    double getTemperature();
}
