package com.alevel.nix.hometasks.lesson11.task2;

public class Water implements Substance {
    private double temperatura = TEMPERATURE;
    @Override
    public State heatUp(double t) {
        temperatura += t;
        if (temperatura >= 100) {
            return State.GAS;
        }else if (temperatura > 0 && temperatura < 100) {
            return State.LIQUID;
        }else {
            return State.SOLID;
        }
    }

    @Override
    public double getTemperature() {
        return temperatura;
    }
}
