package com.alevel.nix.hometasks.lesson13;
@FunctionalInterface
public interface Block<T> {
   void run() throws Exception;
}
