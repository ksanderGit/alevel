package com.alevel.nix.hometasks.lesson13;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Retry<T extends Block> {
    static final Logger logger = LoggerFactory.getLogger(Retry.class);
    public void repeat(T block, int count) throws Exception {
       for (int i = 1; i <= count; i++) {
           try {
               block.run();
               break;
           }catch (Exception e) {
              if (i == count) {
                  logger.error("Exception");
                  throw e;
              }
           }
           try {
               Thread.sleep(100 * i);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }

    public static void main(String[] args) {
        Retry retry = new Retry();
        Block block = () -> {
            if ((int)(Math.random() * 10) == 7) {
                logger.info("code completed successfully");
            }else {
                throw new Exception();
            }
        };

        try {
            retry.repeat(block, 10);
        } catch (Exception e) {
            logger.info("exception caught");
        }
    }
}
