package com.alevel.nix.hometasks.lesson16;

import java.util.*;

public class ForwardLinkedList<E> extends AbstractSequentialList {
    private  int size = 0;
    private  Node<E> first;

    @Override
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    @Override
    public int size() {
        return size;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException();
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    @Override
    public ListIterator listIterator(int index) {
        return new ListItr(index);
    }
    E unlink(ForwardLinkedList.Node<E> x) {
        final E element = x.item;
        final ForwardLinkedList.Node<E> next = x.next;
        final ForwardLinkedList.Node<E> prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        x.item = null;
        size--;
        modCount++;
        return element;
    }
    private void linkFirst(E e) {
        final ForwardLinkedList.Node<E> f = first;
        final ForwardLinkedList.Node<E> newNode = new ForwardLinkedList.Node<>(null, e, f);
        first = newNode;
        if (f != null)
        f.prev = newNode;
        size++;
        modCount++;
    }

    private class ListItr implements ListIterator<E> {
        private ForwardLinkedList.Node<E> lastReturned;
        private ForwardLinkedList.Node<E> next;
        private int nextIndex;
        private int expectedModCount = modCount;

        ListItr(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public E next() {
            checkForComodification();
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public E previous() {
            checkForComodification();
            if (!hasPrevious())
                throw new NoSuchElementException();

            lastReturned = next.prev;
            nextIndex--;
            return lastReturned.item;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            checkForComodification();
            if (lastReturned == null)
                throw new IllegalStateException();

            ForwardLinkedList.Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
            expectedModCount++;
        }

        public void set(E e) {
            if (lastReturned == null)
                throw new IllegalStateException();
            checkForComodification();
            lastReturned.item = e;
        }

        public void add(E e) {
            checkForComodification();
            lastReturned = null;
            if (next == null)
            linkFirst(e);
            nextIndex++;
            expectedModCount++;
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    Node<E> node(int index) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
    }

    private static final class Node<E> {
        E item;
        ForwardLinkedList.Node<E> next;
        ForwardLinkedList.Node<E> prev;

        Node(ForwardLinkedList.Node<E> prev, E element, ForwardLinkedList.Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    public static void main(String[] args) {
        ForwardLinkedList forwardLinkedList = new ForwardLinkedList();
        forwardLinkedList.add("one");
        forwardLinkedList.add("two");
        forwardLinkedList.add("zero");
        System.out.println(forwardLinkedList.size + " " + forwardLinkedList.get(0));
        forwardLinkedList.remove("one");
        System.out.println(forwardLinkedList);
        forwardLinkedList.add("five");
        forwardLinkedList.set(0, "six");
        System.out.println(forwardLinkedList);
    }
}
