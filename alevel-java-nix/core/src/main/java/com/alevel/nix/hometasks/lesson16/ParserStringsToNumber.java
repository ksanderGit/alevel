package com.alevel.nix.hometasks.lesson16;

import java.util.Collection;

public class ParserStringsToNumber {


    public long filterStringsWithStream(Collection<String> list) {
        return list.stream().flatMapToInt(String::codePoints)
                        .filter(ch -> Character.isDigit(ch))
                        .map(Character::getNumericValue)
                        .asLongStream()
                        .reduce((num, digit) -> num * 10 + digit)
                .orElseThrow(() -> new IllegalArgumentException("Input contains no valid digits: " + list));

    }
}
