package com.alevel.nix.hometasks.lesson20;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Weekends {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyMMdd");
    public static List<LocalDate> weekends(String firstDay, String secondDay) {
     LocalDate start = LocalDate.parse(firstDay,FORMATTER);
     LocalDate end = LocalDate.parse(secondDay, FORMATTER);
     System.out.println(ChronoUnit.DAYS.between(start,end));

       return IntStream.rangeClosed(0, (int) ChronoUnit.DAYS.between(start, end))
                .mapToObj(day -> start.plusDays(day))
                .filter(date -> date.getDayOfWeek() == DayOfWeek.SATURDAY ||
                        date.getDayOfWeek() == DayOfWeek.SUNDAY)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(weekends("20200403", "20200701").size());
    }
}
