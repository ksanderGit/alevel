package com.alevel.nix.hometasks.lesson20.hangman;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Dictionary {
    private List<String> dictionary = new ArrayList<>();
    private String word;

    public Dictionary(String path) {
       File file = new File(path);
       readFile(file);
       word = dictionary.get((int) (Math.random() * dictionary.size()));
    }

    public void readFile (File file) {
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s;
            while ((s = reader.readLine())!=null) {
                dictionary.add(s);
            }
        } catch (FileNotFoundException e) {
            throw new UncheckedIOException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    String getWord () {
        return word;
    }
}
