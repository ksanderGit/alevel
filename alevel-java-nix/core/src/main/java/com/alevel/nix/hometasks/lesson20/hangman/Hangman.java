package com.alevel.nix.hometasks.lesson20.hangman;

public class Hangman {
    public static final int ATTEMPTS = 7;
    private int count = 0;
    private String secretWord;
    private String word;

    public Hangman(String word) {
        this.word = word;
        secretWord = getSecretWord();
    }

    public void start () {
        while (gameIsAlive()) {
            System.out.println("Guess any letter in the word");
            System.out.println(secretWord);
            loop(Player.guessTheLetter());
        }
    }

    public void loop(String guess) {
        if (isNumeric(guess) || guess.isEmpty()) {
            System.out.println("Please input character symbol");
            return;
        }
        String result = "";
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == guess.charAt(0)) {
                result += guess.charAt(0);
            } else if (secretWord.charAt(i) != '*') {
                result += word.charAt(i);
            } else {
                result += "*";
            }
        }


        if (secretWord.equals(result)) {
            count++;
            hangmanAnswer();
        } else {
            secretWord = result;
        }
        if (secretWord.equals(word)) {
            System.out.println("Correct! You win! The word was " + word);
        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            if (Character.isDigit(strNum.charAt(0))) {
                return true;
            }
            Double.parseDouble(strNum);
        } catch (StringIndexOutOfBoundsException e) {
            return false;
        }catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean gameIsAlive() {
        if (count < Hangman.ATTEMPTS && secretWord.contains("*")) {
            return true;
        }else {
            return false;
        }
    }

    public void hangmanAnswer() {
        if (count != ATTEMPTS) {
            System.out.println("Wrong guess, try again");
        }else {
            System.out.println("GAME OVER! The word was " + word);
        }
    }
    public String getSecretWord () {
        return new String(new char[word.length()]).replace("\0", "*");
    }

    public int getCount() {
        return count;
    }

}
