package com.alevel.nix.hometasks.lesson20.hangman;

public class Main {
    public static void main(String[] args) {
       Dictionary dictionary = new Dictionary("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/ListWords");
       Hangman hangman = new Hangman(dictionary.getWord());
       hangman.start();
    }
}
