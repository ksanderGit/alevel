package com.alevel.nix.hometasks.lesson20.hangman;

import java.util.Scanner;

public class Player {
    public static String guessTheLetter() {
        Scanner sc = new Scanner(System.in);
        String input = "";
        input = sc.nextLine();
        if (input == null)
        sc.close();
        return input;
    }
}
