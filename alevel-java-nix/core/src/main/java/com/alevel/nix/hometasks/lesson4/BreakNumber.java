package com.alevel.nix.hometasks.lesson4;

import java.util.Deque;
import java.util.LinkedList;

public class BreakNumber {
//    public static void main(String[] args) {
//        BreakNumber partNumber = new BreakNumber();
//        System.out.println(partNumber.split(347_693_485L));
//        System.out.println(partNumber.splitExt(347_693_485L));
//    }
    public String split(long number) {
        String result = "";
        if (number == 0) {
            result = "Input number equals zero";
        }else if (number < 0) {
            result = "Input number below zero";
        }
        long count = 0;
        while (number > 0) {
            count = number%10;
            if (count%2 == 0 && count%3 == 0) {
                result += "fizzbazz ";
            }else if (count%2 == 0) {
                result += "fizz ";
            }else  if (count%3 == 0) {
                result += "bazz ";
            }else {
               result += count + " ";
            }
            number /= 10;
        }
        return result;
    }
    public String splitExt(long number) {
        String result = "";
        Deque<Long> digits = new LinkedList<>();
        if (number == 0) {
            result = "Input number equals zero";
        }else if (number < 0) {
            result = "Input number below zero";
        }else {
            for (; number != 0; number /= 10){
                digits.addFirst(number % 10);
            }
            for (long i : digits) {
                if (i%2 == 0 && i%3 == 0) {
                    result += ("fizzbazz ");
                }else if (i%2 == 0) {
                    result += ("fizz ");
                }else if (i%3 == 0) {
                    result += ("bazz ");
                }else {
                    result += i + " ";
                }
            }
        }
        return result;
    }
}
