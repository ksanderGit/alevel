package com.alevel.nix.hometasks.lesson4;

public class Counter {
    //Long.bitCount()
    public static int counterBits(long number) {
        int count = 0;
        if (number == 0) {
            return 0;
        }

        for(;number != 0; number >>>= 1)
            count += number & 1;
        return count;
    }
}
