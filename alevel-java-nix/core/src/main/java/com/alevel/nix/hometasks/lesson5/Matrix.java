package com.alevel.nix.hometasks.lesson5;

public class Matrix {

    public int[][] transpose(int[][] matrix) {
        if (matrix == null) {
            matrix = new int[3][3];
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i+1; j < matrix.length; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        return matrix;
    }
}
