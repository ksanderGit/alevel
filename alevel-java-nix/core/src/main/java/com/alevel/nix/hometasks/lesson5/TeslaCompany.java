package com.alevel.nix.hometasks.lesson5;

public class TeslaCompany {

    public double maxBenefit(double[] priceList) {
        boolean isEmpty = false;
        int count = 0;
        double buyingPrice = Double.MAX_VALUE;
        double sellingPrice = Double.MIN_VALUE;

        if (priceList == null) {
            return 0.0;
        }

        for (int i = 0, lenght = priceList.length; i < lenght; i++) {
            if (priceList[i] == 0) {
                ++count;
                if (count == lenght) {
                    isEmpty = true;
                }
            }
            if (sellingPrice < priceList[i]) {
                sellingPrice = priceList[i];

            }
            if (buyingPrice > priceList[i]) {
                buyingPrice = priceList[i];
            }
        }

        if (isEmpty) {
            return 0.0;
        }
       double benefit = sellingPrice - buyingPrice;
       return benefit;
    }
}
