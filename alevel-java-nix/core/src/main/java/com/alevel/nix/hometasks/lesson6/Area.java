package com.alevel.nix.hometasks.lesson6;

public class Area {
    public int maxArea(int array[])
    {
        if (array.length < 2) {
            return 0;
        }
        int index = 0;
        int length = array.length - 1;
        int area = 0;

        while (index < length)
        {
            // Calculating the max area
            area = Math.max(area,
                    Math.min(array[index], array[length]) * (length - index));

            if (array[index] < array[length])
                index += 1;

            else
                length -= 1;
        }
        return area;
    }
}
