package com.alevel.nix.hometasks.lesson6;

import java.util.HashSet;
import java.util.Set;

public class LongestUniqSubString {
    public String getUniqueSubString(String input) {
        if (input == null || input == "") {
            return "void string";
        }
        String output = "";

        for (int first = 0, length = input.length(); first < length; first++){
            Set<Character> uniqueChar = new HashSet<>();
            int end = first;
            for (; end < length; end++) {
                char currChar = input.charAt(end);
                if (uniqueChar.contains(currChar)){
                    break;
                }else {
                    uniqueChar.add(currChar);
                }
            }
            if (output.length() < end - first + 1) {
                output = input.substring(first, end);
            }
        }
        return output;
    }
}
