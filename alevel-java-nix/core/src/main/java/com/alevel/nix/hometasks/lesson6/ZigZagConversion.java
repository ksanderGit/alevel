package com.alevel.nix.hometasks.lesson6;

public class ZigZagConversion {
    public String convert(String s, int numRows) {
        if (s == "" || s == null) {
            return "";
        }
        if (numRows <= 1) return s;

        StringBuilder result = new StringBuilder();
        int length = s.length();
        int period = 2 * numRows - 2;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j + i < length; j += period) {
                result.append(s.charAt(j + i));
                if (i != 0 && i != numRows - 1 && j + period - i < length)
                    result.append(s.charAt(j + period - i));
            }
        }
        return result.toString();
    }

    public static String convertAdv(String s, int rows) {
        int length = s.length();
        if (length <= rows || rows < 2) {
            return s;
        }
        var result = new StringBuilder(length);
        int zigzag = (rows << 1) - 2;
        int cursor;
        for (cursor = 0; cursor < length; cursor += zigzag) {
            result.append(s.charAt(cursor));
        }
        int lastRow = rows - 1;
        for (int row = 1; row < lastRow; row++) {
            int zig = (rows - (row + 1)) << 1;
            int zag = row << 1;
            boolean flag;
            for (cursor = row, flag = true; cursor < length; flag = !flag) {
                result.append(s.charAt(cursor));
                cursor += flag ? zig : zag;
            }
        }
        for (cursor = lastRow; cursor < length; cursor += zigzag) {
            result.append(s.charAt(cursor));
        }
        return result.toString();
    }
    public static CharSequence reverse(CharSequence original) {
        return "\u202E" + original.toString();
    }

    public static void main(String[] args) {
        String s = "Hello World!";
        System.out.println(reverse(s));
        //System.out.println(convertAdv("PAYPALISHIRING", 4));
    }

}
