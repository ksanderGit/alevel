package com.alevel.nix.hometasks.lesson7;

public class CharSeq {
    public CharSequence reverse (CharSequence original) {
        if (original.length() < 2) {
            return original;
        }

        return "\u202E" + original;
    }
    public CharSequence subSequence(CharSequence original) {
        if (original.length() < 2) {
            return original;
        }
        char temp;
        byte start = 0;
        int end = original.length()-1;
        char[] arr = original.toString().toCharArray();
        while (start < end)
        {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
        return String.valueOf(arr);
    }
}
