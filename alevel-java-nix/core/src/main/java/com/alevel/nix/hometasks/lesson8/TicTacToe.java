package com.alevel.nix.hometasks.lesson8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TicTacToe {
   static Logger log = LoggerFactory.getLogger(TicTacToe.class);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int [] canvas = {0,0,0,
            0,0,0,
            0,0,0};
    int counter = canvas.length;
    //012, 345, 678, 036, 147, 258, 048, 246
    public static void main(String[] args){
    TicTacToe ticTacToe = new TicTacToe();

    log.info("Start game");
        boolean flag;
        boolean isCurrentX = false;
        int n;
        do {
            isCurrentX = !isCurrentX;
            ticTacToe.drawCanvas();
            System.out.println("input number for mark " + (isCurrentX ? "X" : "O"));
            n = ticTacToe.getNumber();
            if (n == -1) break;
            log.info("Player" + (isCurrentX ? "X" : "O") + " input number " + n);
            try {
                ticTacToe.canvas[n] = isCurrentX ? 1 : 2;
            }catch (ArrayIndexOutOfBoundsException e) {
                log.error(e.getMessage());
            }
            flag = !ticTacToe.isGameOver(n);
        } while (flag);
        ticTacToe.drawCanvas();
        if (n == -1) {
            log.info("Draw");
            System.out.println("Draw");
        }else {
            log.info("The winner is Player" + (isCurrentX ? "X" : "O") + "!");
            System.out.println("The winner is Player" + (isCurrentX ? "X" : "O") + "!");
        }
    }

     int getNumber(){

           while (counter != 0) {
               try {
                   int n = Integer.parseInt(reader.readLine());
                   if (n >= 0 && n < canvas.length && canvas[n]==0) {
                       counter--;
                       return n;
                   }
                   if (n > canvas.length || n < 0) {
                       return n;
                   }
                   log.info("Choose free cell and enter its number");
               } catch (NumberFormatException e) {
                   log.debug("Please enter the number");
               } catch (IOException e) {
                   log.error(e.getMessage());
               }
           }
            return -1;
    }

    public boolean isGameOver(int n) {
        // 0 1 2
        // 3 4 5
        // 6 7 8
        if (n == -1) {
            return false;
        }
        int row = n - n % 3;
        if (canvas[row] == canvas[row + 1] &&
                canvas[row] == canvas[row + 2]) return true;

        int column = n % 3;
        if (canvas[column] == canvas[column + 3])
            if (canvas[column] == canvas[column + 6]) return true;

        if (n % 2 != 0) return false;

        if (n % 4 == 0) {
            if (canvas[0] == canvas[4] &&
                    canvas[0] == canvas[8]) return true;
            if (n != 4) return false;
        }
        return canvas[2] == canvas[4] &&
                canvas[2] == canvas[6];
    }

    public void drawCanvas(){
        System.out.println("     |     |     ");
        for (int i = 0; i < canvas.length; i++) {
            if (i!=0){
                if (i%3==0) {
                    System.out.println();
                    System.out.println("_____|_____|_____");
                    System.out.println("     |     |     ");
                }
                else
                    System.out.print("|");
            }

            if (canvas[i]==0) System.out.print("  " + i + "  ");
            if (canvas[i]==1) System.out.print("  X  ");
            if (canvas[i]==2) System.out.print("  O  ");
        }
        System.out.println();
        System.out.println("     |     |   ");
    }
}
