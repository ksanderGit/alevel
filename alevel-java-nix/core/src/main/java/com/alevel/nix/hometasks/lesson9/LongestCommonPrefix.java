package com.alevel.nix.hometasks.lesson9;

import java.util.Arrays;

public class LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        int size = strs.length;
        if (size == 0) return "";

        if (size == 1)
            return strs[0];

        Arrays.sort(strs);

        /* find the minimum length from first and last string */
        int minStringLen = Math.min(strs[0].length(), strs[size-1].length());

        /* find the common prefix between the first and
           last string */
        int counter = 0;
        while (counter < minStringLen && strs[0].charAt(counter) == strs[size-1].charAt(counter) )
            counter++;

        String prefix = strs[0].substring(0, counter);
        return prefix;
    }

}
