package com.alevel.nix.hometasks.lesson9;

public class MedianOfTwoSortedArr {
   static double findMedianSortedArrays(int[] arrayA, int[] arrayB) {
        int min_index = 0, max_index = 0,
                i = 0, j = 0, median = 0;
        int lengthA = arrayA.length;
        int lengthB = arrayB.length;

        if (lengthA == 0 && lengthB == 0) {
            return 0;
        }

        if (lengthA < lengthB) {
            max_index = lengthA;
        }else {
            max_index = lengthB;
        }
        while (min_index <= max_index)
        {
            i = (min_index + max_index) / 2;
            j = ((lengthA + lengthB + 1) / 2) - i;

            if (i < lengthA && j > 0 && arrayB[j-1] > arrayA[i]) {
                min_index = i + 1;
            }
            else if (i > 0 && j < lengthB && arrayB[j] < arrayA[i-1]) {
                max_index = i - 1;
            }
            else
            {

                if (i == 0) {
                    median = arrayB[j - 1];
                }
                else if (j == 0) {
                    median = arrayA[i - 1];
                }
                else {
                    median = Math.max(arrayA[i - 1], arrayB[j - 1]);
                }
                break;
            }
        }

        if ((lengthA + lengthB) % 2 == 1) {
            return median;
        }

        if (i == lengthA) {
            return (median + arrayB[j]) / 2.0;
        }

        if (j == lengthB) {
            return (median + arrayA[i]) / 2.0;
        }

        return (median + Math.min(arrayA[i], arrayB[j])) / 2.0;
    }
}
