package com.alevel.nix.practicalTasks;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.util.stream.Collectors.*;

public class AggregateDate {

    public Map<LocalDate, SortedSet<LocalTime>> aggregate(List<LocalDateTime> list) {
        if (list.size() == 0) {
            return Collections.EMPTY_MAP;
        }
       return list.stream().collect(groupingBy(LocalDateTime::toLocalDate, mapping(LocalDateTime::toLocalTime, toCollection(TreeSet::new))));
    }


    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        List<LocalDateTime> times = Arrays.asList(
                now.minusHours(2),
                now,
                now.plusDays(1).plusHours(5),
                now.plusDays(2).plusHours(3),
                now.minusDays(1),
                now.plusHours(5),
                now.minusDays(1),
                now.plusHours(3)
        );
        AggregateDate date = new AggregateDate();
        Map<LocalDate, SortedSet<LocalTime>> mapping = date.aggregate(times);

        for(Map.Entry<LocalDate, SortedSet<LocalTime>> item : mapping.entrySet()){

            System.out.println(item.getKey());
            for(LocalTime time : item.getValue()){

                System.out.println(time);
            }
            System.out.println();
        }
        Map.Entry<LocalDate, SortedSet<LocalTime>> entry = mapping.entrySet().iterator().next();
        LocalDate localDate = entry.getKey();
        LocalTime localTime = entry.getValue().first();
        LocalDate localD = (LocalDate) mapping.keySet().toArray()[mapping.size()-1];
        LocalTime localT = mapping.get(localD).first();
        GregorianCalendar gregorianCalendar = new GregorianCalendar(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), localTime.getHour(), localTime.getMinute(), localTime.getSecond());
        GregorianCalendar gregorian = new GregorianCalendar(localD.getYear(), localD.getMonthValue(), localD.getDayOfMonth(), localT.getHour(), localT.getMinute(), localT.getSecond());
        System.out.println((gregorianCalendar.getTimeInMillis() - gregorian.getTimeInMillis())/3600000);


    }
}
