package com.alevel.nix.practicalTasks;

public interface Aggregator<A, T> {
    A aggregate(T[] items);

}
