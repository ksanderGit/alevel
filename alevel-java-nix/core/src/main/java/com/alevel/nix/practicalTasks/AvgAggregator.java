package com.alevel.nix.practicalTasks;

public class AvgAggregator<T extends Number> implements Aggregator<Double, T> {
    @Override
    public Double aggregate(T[] items) {
        double sum = 0;
        if (items.length == 0) {
            return 0.0;
        }

        for (Number item : items) {
            sum += item.doubleValue();
            }
        return sum/items.length;
        }
}
