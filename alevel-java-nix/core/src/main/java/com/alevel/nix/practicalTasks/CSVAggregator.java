package com.alevel.nix.practicalTasks;

import java.util.StringJoiner;

public class CSVAggregator<T> implements Aggregator<String, T> {
    @Override
    public String aggregate(T[] items) {
        if (items.length < 1) {
            return "Array is empty";
        }
        StringJoiner result = new StringJoiner(",");
        for (T item : items) {
            result.add(item.toString());
        }
        return result.toString();
    }
}
