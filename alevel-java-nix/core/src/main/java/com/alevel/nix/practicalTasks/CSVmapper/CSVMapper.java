package com.alevel.nix.practicalTasks.CSVmapper;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVMapper {

    public <T> List<T> mapper(Table table, Class<T> type) {
        try {
            List<Row> rows = table.getRows();

            List<T> result = new ArrayList<>(rows.size());

            Constructor<T> constructor = type.getConstructor();

            Field[] declaredFields = type.getDeclaredFields();

            Map<String, Field> columStringGetField = new HashMap<>(declaredFields.length);
            int counter = 0;
            for (Field declaredField : declaredFields) {
                Column column = declaredField.getAnnotation(Column.class);
                if (column != null && declaredField.trySetAccessible()) {
                    columStringGetField.put(column.column(), declaredField);
                }
            }


            for (Row row : rows) {

                T instance = constructor.newInstance();

                for (Map.Entry<String, Field> e : columStringGetField.entrySet()) {
                    String columnName = e.getKey();
                    Field field = e.getValue();

                    int index = table.getColumnIndex(columnName);

                    Cell cell = row.get(index);

                    field.set(instance, cell.getValue());
                    result.add(instance);

                }
            }

            return result;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
