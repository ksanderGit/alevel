package com.alevel.nix.practicalTasks.CSVmapper;

public interface CSVParser<T> {
    Table parse(T path) throws CSVException;
}
