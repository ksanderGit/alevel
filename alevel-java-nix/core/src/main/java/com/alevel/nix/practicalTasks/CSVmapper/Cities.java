package com.alevel.nix.practicalTasks.CSVmapper;

public class Cities {
    @Column(column = "Number")
    private String number;
    @Column(column = "Cod")
    private String code;
    @Column(column = "Country")
    private String country;

    public String getNumber() {
        return number;
    }

    public String getCode() {
        return code;
    }

    public String getCountry() {
        return country;
    }
}
