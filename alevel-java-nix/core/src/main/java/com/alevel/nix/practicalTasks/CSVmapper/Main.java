package com.alevel.nix.practicalTasks.CSVmapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try {
            CSVParser parser = new ParserCSV();
            File file = new File(args[0]);
            Table table = parser.parse(file.toPath());
            CSVMapper map = new CSVMapper();
            List<Cities> cities = map.mapper(table, Cities.class);
            for (Cities city : cities){
                System.out.println(city.getNumber() + " " + city.getCode() + " " + city.getCountry());
            }
        }catch (CSVException e) {
            throw new UnsupportedOperationException(e);
        }

    }
}
