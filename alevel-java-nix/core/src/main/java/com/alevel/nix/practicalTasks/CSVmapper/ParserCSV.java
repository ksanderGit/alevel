package com.alevel.nix.practicalTasks.CSVmapper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class ParserCSV implements CSVParser<Path> {
    @Override
    public Table parse(Path path) throws CSVException {
        Table table = null;
        try(BufferedReader reader = Files.newBufferedReader(path)) {
            String header = reader.readLine();
            if (header == null) {
                throw new CSVException("file is empty");
            }
            table = new Table(parser(header));
            String line;
            while ((line = reader.readLine()) != null) {
                table.addRow(parser(line));
            }

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return table;
    }

    private Row parser(String line) {
        return new Row(Arrays.asList(line.split(",")));
    }
}
