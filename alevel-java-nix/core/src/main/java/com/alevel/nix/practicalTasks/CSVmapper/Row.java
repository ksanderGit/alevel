package com.alevel.nix.practicalTasks.CSVmapper;

import java.util.ArrayList;
import java.util.List;

public class Row {
    private final List<Cell> cells;

    public Row(List<String> cells) {
        this.cells = new ArrayList<>(cells.size());
        for (String cell: cells) {
            this.cells.add(new Cell(cell));
        }
    }

    public int size() {
        return cells.size();
    }

    public Cell get(int index) {
        return cells.get(index);
    }
}
