package com.alevel.nix.practicalTasks.CSVmapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Table {
    private final Map<String, Integer> header;
    private final List<Row> rows;

    public Table(Row headers) {
        header = new HashMap<>(headers.size());
        for (int i = 0; i < headers.size(); i++) {
            this.header.put(headers.get(i).getValue(), i);
        }
        this.rows = new ArrayList<>();
    }

    public Map<String, Integer> getHeader() {
        return header;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void addRow(Row row) {
        rows.add(row);
    }

    public int getColumnIndex(String columnName) {
        return header.get(columnName);
    }
}
