package com.alevel.nix.practicalTasks;

import java.util.Arrays;
import java.util.HashSet;

public class DistinctAggregator<T extends Object> implements Aggregator<Integer, T> {
    @Override
    public Integer aggregate(T[] items) {
        if (items.length < 1) {
            return 0;
        }
        return new HashSet<>(Arrays.asList(items)).size();
    }
}
