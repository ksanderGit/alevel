package com.alevel.nix.practicalTasks;

public class MaxAggregator<T extends Comparable> implements Aggregator<Object, T> {

    @Override
    public Object aggregate(T[] items) {
       if (items.length < 1) {
           return 0;
       }
       Object max = items[0];
       for (int i = 1; i < items.length; i++) {
           if (items[i].compareTo(max) > 0) {
               max = items[i];
           }
       }
       return max;
    }
}
