package com.alevel.nix.practicalTasks;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadCSV {
    private static String line;
    private static final String csvSplit = ",";
    private static List<ArrayList<String>> lists = new ArrayList<>();
    private static String[] headers;

    public ReadCSV(File file) {
        read(file);
    }

    public static List<ArrayList<String>> read (File file) {
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            headers = reader.readLine().split(csvSplit);

            for (int i = 0; i < headers.length; i++) {
                ArrayList<String> array = new ArrayList<>();
                array.add(headers[i]);
                lists.add(array);
            }
            while ((line = reader.readLine()) != null) {
                String[] array = line.split(csvSplit);
                for (int i = 0; i < array.length; i++) {
                    lists.get(i).add(array[i]);
                }
            }
        }catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return lists;
    }

    public String searchByRowAndColumNumber(int row, int colum) {
        if (row < 0 || colum < 0) {
            return "Row else colum can't be negative";
        }
        return lists.get(colum).get(row);
    }

    public String searchByRowNumberAndColumName(int row, String header) {
        if (row < 0 || header == null) {
            return "Invalid input parameters";
        }
        int index;
        for (index = 0; index < headers.length-1; index++) {
            if (header.equals(headers[index])) {
                break;
            }
        }
        return searchByRowAndColumNumber(row, index);
    }

    public String[] getHeaders() {
        return headers;
    }
}
