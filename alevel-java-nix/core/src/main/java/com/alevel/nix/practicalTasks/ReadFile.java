package com.alevel.nix.practicalTasks;

import java.io.*;

public class ReadFile {
    public void readFile(File file, String check) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (reader.ready()) {
                String result = reader.readLine();
                if (result.contains(check)) {
                    System.out.println(result);
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void main(String[] args) {
        ReadFile readFile = new ReadFile();
        File file = new File("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/logback.xml");
        readFile.readFile(file, "appender");
    }
}
