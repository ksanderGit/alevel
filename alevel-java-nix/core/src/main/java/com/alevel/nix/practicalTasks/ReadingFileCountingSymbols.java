package com.alevel.nix.practicalTasks;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class ReadingFileCountingSymbols {

    public static Map<Character, Integer> readFile(File file) {
        Map<Character, Integer> frequency = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            Character symbol;
            while (reader.ready()) {
                symbol = (char)reader.read();
                frequency.merge(symbol, 1, Integer::sum);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return frequency;
    }
    public static void main(String[] args) {
        File file = new File("/Users/macbookair/Desktop/char.txt");
        Map<Character, Integer> map = readFile(file);
        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
