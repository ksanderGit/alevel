package com.alevel.nix.practicalTasks;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class RecursiveCopyDirectory {

    public static void copyFolder(File sourceFolder, File destinationFolder) throws IOException
    {
        if (sourceFolder.isDirectory())
        {
            if (!destinationFolder.exists())
            {
                destinationFolder.mkdir();
            }
          String[] files = sourceFolder.list();

            for (String file : files)
            {
                File srcFile = new File(sourceFolder, file);
                File destFile = new File(destinationFolder, file);

                copyFolder(srcFile, destFile);
            }
        }
        else
        {
            Files.copy(sourceFolder.toPath(), destinationFolder.toPath());
        }
    }

    public static void main(String[] args) {
        File source = new File("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/java");
        File dest = new File("/Users/macbookair/Desktop/newFolder");
        try {
            copyFolder(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
