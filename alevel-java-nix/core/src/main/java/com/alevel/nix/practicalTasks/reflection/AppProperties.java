package com.alevel.nix.practicalTasks.reflection;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Properties;

public class AppProperties {
    public int size;
    public String name;
    @PropertyKey
    private int connection;

    private static Properties loadProperties() {

        Properties props = new Properties();
        try(InputStream input = AppProperties.class.getResourceAsStream("app.properties")) {
            props.load(input);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return props;
    }
    public static int getConnection() {
        Properties properties = loadProperties();
       return Integer.parseInt(properties.getProperty("connections.limit"));
    }

    public static void main(String[] args) {
        AppProperties properties = new AppProperties();
        try {
            Class<? extends AppProperties> classOfInstance = AppProperties.class;
            Field connect = classOfInstance.getDeclaredField("connection");
            PropertyKey key = connect.getAnnotation(PropertyKey.class);
            if (key == null) return;

            if (key.maxConnections() == 1) {
               connect.set(properties, getConnection());
                System.out.println(properties.connection);
            }

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
