package com.alevel.nix.programmingday;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class CachingKeyPackingDateWizard extends SimpleDateWizard {
    private final Map<Long, LocalDate> cache = new HashMap<>();

//    @Override
//    public LocalDate getDateOfYear(int year, int day) {
//        return cache.computeIfAbsent(key(year, day), key -> super.getDateOfYear(year, day));
//    }

    private static long key(int year, int day) {
        long key = year <<= 32;
         key+= (day & 0xffffffff);
        return key;
    }

    static int countSetBits(int n)
    {
        int count = 0;
        while (n > 0) {
            System.out.println(Integer.toBinaryString(n));
            count+= n & 1;
            n >>= 1;
        }
        return count;
    }

    static void modulo(int n) {
        int count = n;
        while (count > 10) {
            System.out.println(n%10);
            count /= 10;
        }
        System.out.println(count);
    }

    public static void main(String[] args) {
        //System.out.println(key( 2,  -5));
       modulo(255);
    }

}
