package com.alevel.nix.programmingday;

import java.time.LocalDate;

public interface DateWizard {
    LocalDate getDateOfYear(int year, int day);
}
