package module.level1;

import java.util.ArrayList;
import java.util.List;

public class DateValidator {
    public List<String> validData(List<String> dates) {
        List<String> listDates = new ArrayList<>();
        for (String date : dates) {
            StringBuilder sb = new StringBuilder();
            if (date.contains("/")) {
                String[] arrayStrings = date.split("/");
                if (arrayStrings[0].length() < 3) {
                    for (int i = arrayStrings.length-1; i >=0 ; i--) {
                        sb.append(arrayStrings[i]);
                    }
                    listDates.add(sb.toString());
                }else {
                    for (String s : arrayStrings) {
                        sb.append(s);
                    }
                    listDates.add(sb.toString());
                }
            }else if (date.contains("-")) {
                String[] arrayStrings = date.split("-");
                if (arrayStrings[0].length() < 3) {
                    for (int i = arrayStrings.length-1; i >=0 ; i--) {
                        sb.append(arrayStrings[i]);
                    }
                    listDates.add(sb.toString());
                }else {
                    for (String s : arrayStrings) {
                        sb.append(s);
                    }
                    listDates.add(sb.toString());
                }
            }
        }
        return listDates;
    }

    public static void main(String[] args) {
        List dates = new ArrayList();
        dates.add("2020/04/05");
        dates.add("09/04/2020");
        dates.add("04-05-2020");
        dates.add("2020-05-05");
        dates.add("2020,04,03");
        DateValidator dateValidator = new DateValidator();
        System.out.println(dateValidator.validData(dates).toString());
    }
}
