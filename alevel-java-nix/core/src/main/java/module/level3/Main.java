package module.level3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Node> nodes = ReadingAndWritingFile.readFile("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/Towns.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Node src = null;
        Node dest = null;
        String sourceNode = "";
        String destination = "";
        try {
            System.out.println("Input your location");
            sourceNode = reader.readLine();
            System.out.println("Input destination");
            destination = reader.readLine();
            for (Node node : nodes) {
                if (node.getName().equals(sourceNode)) {
                    src = node;
                }else if (node.getName().equals(destination)) {
                    dest = node;
                }
            }
            if (src == null || dest == null) {
                throw new NullPointerException();
            }
        }catch (NullPointerException e) {
            System.out.println("Location isn't found");
            System.exit(1);
        }
        catch (IOException e) {
           throw new UncheckedIOException(e);
        }
        Node destinationNode = OptimalWay.calculateShortestPathFromSource(dest, src);
        ReadingAndWritingFile.writeFile(sourceNode, destination, destinationNode.getDistance());
    }
}
