package module.level3;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class OptimalWay {
    public static Node calculateShortestPathFromSource(Node destination, Node source) {
        source.setDistance(0);

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);
        while (unsettledNodes.size() != 0) {
            Node currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Map.Entry< Node, Integer> adjacencyPair:
                    currentNode.getAdjacentNodes().entrySet()) {
                Node neigbor = adjacencyPair.getKey();
                Integer costWay = adjacencyPair.getValue();
                if (!settledNodes.contains(neigbor)) {
                    calculateMinimumDistance(neigbor, costWay, currentNode);
                    unsettledNodes.add(neigbor);
                }
            }
            settledNodes.add(currentNode);
        }
        return destination;
    }

    private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node : unsettledNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private static void calculateMinimumDistance(Node neighbor, Integer costWay, Node sourceNode) {
        Integer sourceDistance = sourceNode.getDistance();
        if (sourceDistance + costWay < neighbor.getDistance()) {
            neighbor.setDistance(sourceDistance + costWay);
        }
    }
}
