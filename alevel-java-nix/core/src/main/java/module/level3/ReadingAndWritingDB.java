package module.level3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.sql.*;
import java.util.*;

public class ReadingAndWritingDB {
private static List<Node> nodes = new ArrayList<>();
    public static List<Node> readingFromDB(Properties properties) {

        try (Connection connection = DriverManager.getConnection(properties.getProperty("url"), properties)) {
            try (PreparedStatement getSities = connection.prepareStatement(
                    "SELECT * FROM cities")) {

                ResultSet resultSet = getSities.executeQuery();
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    Integer id = resultSet.getInt("id");
                    nodes.add(new Node(name));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try (PreparedStatement getActualNeighbors = connection.prepareStatement(
                    "SELECT * FROM connection")) {

                ResultSet resultSet = getActualNeighbors.executeQuery();
                while (resultSet.next()) {
                    Integer from = resultSet.getInt("from");
                    Integer to = resultSet.getInt("to");
                    Integer cost = resultSet.getInt("cost");
                    for (int i = 0; i <= nodes.size(); i++) {
                        if (i == (from-1)) {
                            nodes.get(i).setNeighbors(nodes.get(i).getNeighbors() + 1);
                            nodes.get(i).addDestination(nodes.get(to-1), cost);
                        }
                    }
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return nodes;
    }
    public static void writingIntoDB(Properties properties) {
        try(Connection connection = DriverManager.getConnection(properties.getProperty("url"), properties)) {
            try(PreparedStatement readFromProblemDB = connection.prepareStatement("SELECT * FROM problems")) {
                ResultSet setProblems = readFromProblemDB.executeQuery();
                while (setProblems.next()) {
                    int from = setProblems.getInt("from");
                    int to = setProblems.getInt("to");
                    try(PreparedStatement writeToFound_RoutesDB = connection.prepareStatement("INSERT INTO found_Routes (id, cost) VALUES (?,?)")){
                        Node shortestPath = OptimalWay.calculateShortestPathFromSource(nodes.get(from-1), nodes.get(to-1));
                        writeToFound_RoutesDB.setInt(1, setProblems.getInt("id"));
                        writeToFound_RoutesDB.setInt(2,shortestPath.getDistance());
                    }catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
//            try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM found_Routes")) {
//               ResultSet set = statement.executeQuery();
//               while (set.next()) {
//                   System.out.println(set.getInt(1));
//                   System.out.println(set.getInt(2         ));
//               }
//            }catch (SQLException e) {
//                throw new RuntimeException(e);
//            }
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
