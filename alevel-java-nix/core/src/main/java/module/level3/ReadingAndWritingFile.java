package module.level3;

import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class ReadingAndWritingFile {
    public  static List<Node> readFile (String path) {
        File file = new File(path);
        List<Node> nodes;
        Queue<String> listNeighbors = new ArrayDeque<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            nodes = new ArrayList<>(Integer.parseInt(reader.readLine()));
            String result;
            while ((result = reader.readLine()) != null) {
                Node node = new Node(result);
                nodes.add(node);
                int neighbors = Integer.parseInt(reader.readLine());
                node.setNeighbors(neighbors);
                while (neighbors > 0) {
                    listNeighbors.add(reader.readLine());
                    neighbors--;
                }
            }
        }catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        for (Node node : nodes) {
            int negihbors = node.getNeighbors();
            while (negihbors > 0) {
                String[] s = listNeighbors.remove().split(" ");
                node.addDestination(nodes.get(Integer.parseInt(s[0]) - 1), Integer.parseInt(s[1]));
                negihbors--;
            }
        }
        return nodes;
    }

    public static void writeFile(String location, String destination, int cost) {
        File file = new File("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/output.txt");
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("Minimum way from g");
            writer.append(location);
            writer.append(" -> ");
            writer.append(destination);
            writer.append(" costs ");
            writer.append(cost + "");
            writer.flush();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
