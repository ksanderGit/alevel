package com.alevel.nix.hometasks.lesson10.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TicTacToe3x3Test {
    TicTacToe3x3 ticTac;
    @BeforeEach
    void setUp() {
        ticTac = new TicTacToe3x3();
    }

    @Test
    void mark() {
        ticTac.canvas[3] = 1;
        boolean actual = ticTac.mark(3);
        assertEquals(false, actual);
        actual = ticTac.mark(10);
        assertEquals(false, actual);
        ticTac.canvas[1] = 2;
        actual = ticTac.mark(1);
        assertEquals(false, actual);
        ticTac.canvas[4] = 1;
        actual = ticTac.mark(4);
        assertEquals(false, actual);
        ticTac.canvas[6] = 2;
        actual = ticTac.mark(6);
        assertEquals(false, actual);
        ticTac.canvas[5] = 1;
        assertEquals(true, ticTac.mark(5));
    }

    @Test
    void getNumber() throws IOException {
        ticTac.reader= Mockito.mock(BufferedReader.class);
        Mockito.when(ticTac.reader.readLine()).thenReturn("3");
        int actual = ticTac.getNumber();
        assertEquals(3, actual);
        Mockito.when(ticTac.reader.readLine()).thenReturn("10");
        actual = ticTac.getNumber();
        assertEquals(10, actual);
    }
}