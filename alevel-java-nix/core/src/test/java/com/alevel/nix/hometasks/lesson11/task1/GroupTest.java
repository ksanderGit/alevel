package com.alevel.nix.hometasks.lesson11.task1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
class GroupTest {
    Group group;
    @BeforeEach
    void setUp() {
        group = new Group();
    }

    @Test
    void getContractStudents() {
        Student[] expected = new Student[5];
        expected[0] = new ContractStudent("Zhora", 22, 5300);
        expected[1] = new ContractStudent("Dima", 23, 7000);
        expected[2] = new ContractStudent("Sveta", 18, 6300);
        expected[3] = new ContractStudent("Valera", 25, 8500);
        expected[4] = new ContractStudent("Vasya", 22, 3500);
        List<Student> actual = group.getContractStudents(createGroup());

        assertArrayEquals(expected, actual.toArray());

    }

    private Student[] createGroup() {
        Student[] students = new Student[10];
        students[0] = new Student("Dasha", 19);
        students[1] = new Student("Petya", 20);
        students[3] = new ContractStudent("Zhora", 22, 5300);
        students[4] = new ContractStudent("Dima", 23, 7000);
        students[5] = new Student("Igor", 24);
        students[6] = new ContractStudent("Sveta", 18, 6300);
        students[7] = new ContractStudent("Valera", 25, 8500);
        students[8] = new Student("Stas", 21);
        students[9] = new ContractStudent("Vasya", 22, 3500);
        return students;
    }
}