package com.alevel.nix.hometasks.lesson11.task2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class IronTest {
    Iron iron;
    @BeforeEach
    void setUp() {
        iron = new Iron();
    }

    @Test
    void heatUp() {
       State expected = State.SOLID;
       State actual = iron.heatUp(1000.90);
       assertEquals(expected, actual);

       assertEquals(State.LIQUID, iron.heatUp(650.7));

       assertEquals(State.GAS, iron.heatUp(2040.5));
    }
}