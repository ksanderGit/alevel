package com.alevel.nix.hometasks.lesson11.task2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OxygenTest {
    Oxygen oxygen;
    @BeforeEach
    void setUp() {
        oxygen = new Oxygen();
    }

    @Test
    void heatUp() {
        State expected = State.GAS;
        State actual = oxygen.heatUp(-50);
        assertEquals(expected, actual);

        assertEquals(State.LIQUID, oxygen.heatUp(-180));

        assertEquals(State.SOLID, oxygen.heatUp(-300.2));
    }
}