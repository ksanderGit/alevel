package com.alevel.nix.hometasks.lesson11.task2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WaterTest {
    Water water;
    @BeforeEach
    void setUp() {
        water = new Water();
    }

    @Test
    void heatUp() {
        State expected = State.SOLID;
        State actual = water.heatUp(-30.3);
        assertEquals(expected, actual);

        assertEquals(State.LIQUID, water.heatUp(50));

        assertEquals(State.GAS, water.heatUp(90));
    }
}