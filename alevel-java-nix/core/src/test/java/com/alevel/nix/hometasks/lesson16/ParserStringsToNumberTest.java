package com.alevel.nix.hometasks.lesson16;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ParserStringsToNumberTest {
    private ParserStringsToNumber subject;

    @BeforeEach
    void setUp() {
        subject  = new ParserStringsToNumber();
    }


    @Test
    void filterStringsWithStream() {

        assertEquals(12345, subject.filterStringsWithStream(List.of("string 1 text", "2 string 3 text", "45")));
        assertEquals(45, subject.filterStringsWithStream(List.of("string 0 text", "0 string 0 text", "45")));
        assertEquals(0, subject.filterStringsWithStream(List.of("string 0 text", "0 string 0 text", "00")));
        assertEquals(10000, subject.filterStringsWithStream(List.of("string 1 text", "0 string 0 text", "00")));

    }


    @Test
    void whenStringsContainNoDigits_ShouldThrowIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> subject.filterStringsWithStream(List.of()));
        assertThrows(IllegalArgumentException.class, () -> subject.filterStringsWithStream(List.of("s")));
        assertThrows(IllegalArgumentException.class, () -> subject.filterStringsWithStream(List.of("s-one", "s-two")));
    }

}