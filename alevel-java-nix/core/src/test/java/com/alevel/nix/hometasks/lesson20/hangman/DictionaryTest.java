package com.alevel.nix.hometasks.lesson20.hangman;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.UncheckedIOException;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

class DictionaryTest {

    @Test
    void getWord() {
       Dictionary dictionary = Mockito.mock(Dictionary.class);
       doThrow(UncheckedIOException.class).when(dictionary).readFile(new File(""));
       doNothing().when(dictionary).readFile(isA(File.class));
       dictionary.readFile(new File("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/ListWords"));
       verify(dictionary).readFile(new File("/Users/macbookair/IntelijIdea/Alevel/alevel-java-nix/core/src/main/resources/ListWords"));
    }
}