package com.alevel.nix.hometasks.lesson20.hangman;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


class HangmanTest {
Hangman hangman = new Hangman("sport");
    @Test
    void loop() {
       assertEquals(getAnswer("0"), "Please input character symbol");
       assertEquals(getAnswer("-1"), "Please input character symbol");
       assertEquals(getAnswer("i"), "Wrong guess, try again");
       assertEquals(getAnswer("q"), "Wrong guess, try again");
       assertEquals(getAnswer("y"), "Wrong guess, try again");
       assertEquals(getAnswer("k"), "Wrong guess, try again");
       assertEquals(getAnswer("n"), "Wrong guess, try again");
       assertEquals(getAnswer("z"), "Wrong guess, try again");
       assertEquals(getAnswer("a"), "GAME OVER! The word was sport");
       assertEquals(getAnswer("s"), "");
       assertEquals(getAnswer("p"), "");
       assertEquals(getAnswer("o"), "");
       assertEquals(getAnswer("r"), "");
       assertEquals(getAnswer("t"), "Correct! You win! The word was sport");
    }

    private String getAnswer(String s) {
        String result = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            PrintStream old = System.out;
            System.setOut(ps);
            hangman.loop(s);
            System.out.flush();
            result = baos.toString();
            System.setOut(old);
            baos.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
       return result.trim();
    }
}