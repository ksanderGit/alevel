package com.alevel.nix.hometasks.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BreakNumberTest {
    private BreakNumber breakNumber;
    @BeforeEach
    void setUp() {
        breakNumber = new BreakNumber();
    }

    @Test
    void split() {
        assertSplit("Input number equals zero", 0);
        assertSplit("Input number below zero", -5);
        assertSplit("5 fizz fizz bazz bazz fizzbazz 7 fizz bazz", 347_693_485L);
    }

    private void assertSplit(String expected, long actual) {
        String act = breakNumber.split(actual).trim();
        assertEquals(expected, act);
    }

    @Test
    void splitExt() {
        assertSplitExt("Input number equals zero", 0);
        assertSplitExt("Input number below zero", -5);
        assertSplitExt("bazz fizz 7 fizzbazz bazz bazz fizz fizz 5", 347_693_485L);
    }

    private void assertSplitExt(String expected, long actual) {
        String act = breakNumber.splitExt(actual).trim();
        assertEquals(expected, act);
    }
}