package com.alevel.nix.hometasks.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CounterTest {
    private Counter counter;
    @BeforeEach
    void setUp() {
        counter = new Counter();
    }

    @Test
    void counterBits() {
        testCounter(2, counter.counterBits(5));
        testCounter(0, counter.counterBits(0));
        testCounter(64, counter.counterBits(-1));
    }

    private void testCounter(int expected, int actual) {
        assertEquals(expected, actual);
    }
}