package com.alevel.nix.hometasks.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    Matrix matrix;
    @BeforeEach
    void setUp() {
        matrix = new Matrix();
    }

    @Test
    void transpose() {
        int[][] array = new int[][] {
                {3, 2, 1},
                {7, 6, 5},
                {0, 9, 8}
        };
        int [][] expected = new int[][] {
                {3, 7, 0},
                {2, 6, 9},
                {1, 5, 8}
        };
        int [][] expectedForNull = new int[3][3];
        int [][] actual = matrix.transpose(array);
        testTranspose(actual, expected);
        testTranspose(matrix.transpose(new int[3][3]), expectedForNull);
    }

    private void testTranspose(int[][] actual, int [][] expected) {
        assertArrayEquals(expected, actual);
    }
}