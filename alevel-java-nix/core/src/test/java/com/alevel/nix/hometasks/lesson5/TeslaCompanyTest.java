package com.alevel.nix.hometasks.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeslaCompanyTest {

    TeslaCompany teslaCompany;
    @BeforeEach
    void setUp() {
        teslaCompany = new TeslaCompany();
    }

    @Test
    void maxBenefit() {
    double[] priceList = {55.3, 130.5, 48.1, 89.0, 10.5, 90.2, 70.3};
    double expected = 120;
    testMaxBenefit(expected, teslaCompany.maxBenefit(priceList));
    testMaxBenefit(0.0, teslaCompany.maxBenefit(null));
    testMaxBenefit(0.0, teslaCompany.maxBenefit(new double[7]));
    }

    private void testMaxBenefit(double expected, double actual) {
        assertEquals(expected, actual);
    }
}