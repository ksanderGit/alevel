package com.alevel.nix.hometasks.lesson6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AreaTest {
    Area area;
    @BeforeEach
    void setUp() {
       area = new Area();
    }

    @Test
    void maxArea() {
        int actual = area.maxArea(new int[]{1,8,6,2,5,4,8,3,7});
        int expected = 49;
        testMaxArea(actual, expected);
        testMaxArea(area.maxArea(new int[]{1}), 0);
    }

    private void testMaxArea(int actual, int expected) {
        assertEquals(expected, actual);
    }
}