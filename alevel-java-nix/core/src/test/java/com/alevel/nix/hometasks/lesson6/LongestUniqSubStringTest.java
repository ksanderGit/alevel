package com.alevel.nix.hometasks.lesson6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestUniqSubStringTest {
    LongestUniqSubString longSubStr;
    @BeforeEach
    void setUp() {
        longSubStr = new LongestUniqSubString();
    }

    @Test
    void getUniqueSubString() {
        String actual = longSubStr.getUniqueSubString("pwwkew");
        String expected = "kew";
        testUniqueSrt(actual, expected);
        testUniqueSrt(longSubStr.getUniqueSubString(""), "void string");
    }

    private void testUniqueSrt(String actual, String expected) {
        assertEquals(expected, actual);
    }
}