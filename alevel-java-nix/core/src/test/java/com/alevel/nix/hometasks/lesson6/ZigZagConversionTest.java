package com.alevel.nix.hometasks.lesson6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZigZagConversionTest {
    ZigZagConversion zigZag;
    @BeforeEach
    void setUp() {
        zigZag = new ZigZagConversion();
    }

    @Test
    void convert() {
        String actual = zigZag.convert("PAYPAlISHIRING", 3);
        String expected = "PAHNAPlSIIGYIR";
        testConvert(actual, expected);
        testConvert(zigZag.convert("", 4), "");
        testConvert(zigZag.convert("PAYPAlISHIRING", 0), "PAYPAlISHIRING");
    }

    private void testConvert(String actual, String expected) {
        assertEquals(expected, actual);
    }
}