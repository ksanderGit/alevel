package com.alevel.nix.hometasks.lesson7;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CharSeqTest {
    CharSeq charSeq;
    @BeforeEach
    void setUp() {
        charSeq = new CharSeq();
    }

    @Test
    void reverse() {
        CharSequence actual = charSeq.subSequence("hello java");
        CharSequence act = charSeq.reverse("hello java");
        assertEquals("avaj olleh", actual);
        assertEquals("avaj olleh", act);
        assertEquals("h", "h");
    }
}