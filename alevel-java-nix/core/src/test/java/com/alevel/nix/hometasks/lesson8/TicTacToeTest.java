package com.alevel.nix.hometasks.lesson8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TicTacToeTest {
    TicTacToe ticTac;
    @BeforeEach
    void setUp() {
        ticTac = new TicTacToe();
    }

    @Test
    void getNumber() throws IOException {
        ticTac.reader= Mockito.mock(BufferedReader.class);
        Mockito.when(ticTac.reader.readLine()).thenReturn("3");
        int actual = ticTac.getNumber();
        assertEquals(3, actual);
        Mockito.when(ticTac.reader.readLine()).thenReturn("10");
        actual = ticTac.getNumber();
        assertEquals(10, actual);
    }

    @Test
    void isGameOver() {
        // 0 1 2
        // 3 4 5
        // 6 7 8
        // set X - 1
        // set O - 2
        ticTac.canvas[3] = 1;
        boolean actual = ticTac.isGameOver(3);
        assertEquals(false, actual);
        ticTac.canvas[1] = 2;
        actual = ticTac.isGameOver(1);
        assertEquals(false, actual);
        ticTac.canvas[4] = 1;
        actual = ticTac.isGameOver(4);
        assertEquals(false, actual);
        ticTac.canvas[6] = 2;
        actual = ticTac.isGameOver(6);
        assertEquals(false, actual);
        ticTac.canvas[5] = 1;
        assertEquals(true, ticTac.isGameOver(5));
    }
}