package com.alevel.nix.hometasks.lesson9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestCommonPrefixTest {
    LongestCommonPrefix commonPrefix;
    @BeforeEach
    void setUp() {
        commonPrefix = new LongestCommonPrefix();
    }

    @Test
    void longestCommonPrefix() {
        String[] strs = new String[]{"reset","return","rebook"};
        String expected = "re";
        String actual = commonPrefix.longestCommonPrefix(strs);
        assertEquals(expected, actual);

        strs = new String[]{"flower","flow","flight, float"};
        expected = "fl";
        actual = commonPrefix.longestCommonPrefix(strs);
        assertEquals(expected, actual);

        assertEquals("car", commonPrefix.longestCommonPrefix(new String[]{"car"}));
        assertEquals("", commonPrefix.longestCommonPrefix(new String[]{}));
        assertEquals("", commonPrefix.longestCommonPrefix(new String[]{"green", "yellow", "grey"}));
    }
}