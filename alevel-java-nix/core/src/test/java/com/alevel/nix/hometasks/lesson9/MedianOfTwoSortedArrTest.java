package com.alevel.nix.hometasks.lesson9;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MedianOfTwoSortedArrTest {
    MedianOfTwoSortedArr median;
    @BeforeEach
    void setUp() {
        median = new MedianOfTwoSortedArr();
    }

    @Test
    void findMedianSortedArrays() {
        double expected = 15.0;
        double actual = median.findMedianSortedArrays(new int[]{6, 9, 15, 39, 60}, new int[]{2, 10, 31, 65});
        assertEquals(expected, actual);
        assertEquals(0, median.findMedianSortedArrays(new int[]{}, new  int[]{}));
        assertEquals(4.5, median.findMedianSortedArrays(new int[]{}, new int[]{1,2,3,4,5,6,7,8}));
    }
}