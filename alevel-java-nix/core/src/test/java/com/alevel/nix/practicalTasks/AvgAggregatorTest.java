package com.alevel.nix.practicalTasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvgAggregatorTest {

    @Test
    void aggregate() {
        AvgAggregator aggregator = new AvgAggregator();
        Number[] array = new Number[]{1.5, 2.2, 3.1, 4.2, 5, 6.3, 7};
        assertEquals(0, aggregator.aggregate(new Number[]{}));
        assertEquals(4.185714285714286, aggregator.aggregate(array));
    }
}