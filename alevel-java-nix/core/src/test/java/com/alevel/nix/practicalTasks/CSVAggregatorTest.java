package com.alevel.nix.practicalTasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVAggregatorTest {

    @Test
    void aggregate() {
        CSVAggregator csvAggregator = new CSVAggregator();
        String expected = "try,dry,final,1,3,5,true";
        assertEquals(expected, csvAggregator.aggregate(new Object[]{"try", "dry", "final", 1, 3, 5, Boolean.TRUE}));
        assertEquals("Array is empty", csvAggregator.aggregate(new Object[]{}));
    }
}