package com.alevel.nix.practicalTasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistinctAggregatorTest {

    @Test
    void aggregate() {
        DistinctAggregator distinctAggregator = new DistinctAggregator();
        assertEquals(6, distinctAggregator.aggregate(new Object[]{1, "string", 2.0, Boolean.TRUE, 2, 'c', "string", 2.0, Boolean.TRUE, 2}));
        assertEquals(0, distinctAggregator.aggregate(new Object[]{}));
    }
}