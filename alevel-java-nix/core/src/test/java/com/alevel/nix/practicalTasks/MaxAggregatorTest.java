package com.alevel.nix.practicalTasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxAggregatorTest {

    @Test
    void aggregate() {
        MaxAggregator aggregator = new MaxAggregator();
        assertEquals(0, aggregator.aggregate(new Integer[]{}));
        assertEquals(7, aggregator.aggregate(new Integer[]{1, 2, 3, 7, 5}));
        assertEquals("yellow", aggregator.aggregate(new String[]{"red", "blue", "green", "yellow"}));
    }
}