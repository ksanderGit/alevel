package com.alevel.nix.practicalTasks;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class ReadCSVTest {
    ReadCSV readCSV;
    @BeforeEach
    void setUp() {
        readCSV = new ReadCSV(new File("/Users/macbookair/Desktop/Country.csv"));
    }

    @Test
    void searchByRowAndColumNumber() {
        assertEquals("Row else colum can't be negative", readCSV.searchByRowAndColumNumber(-1, -2));
        assertEquals("AU", readCSV.searchByRowAndColumNumber(3, 1));
        assertEquals("Number", readCSV.searchByRowAndColumNumber(0,0));
    }

    @Test
    void searchByRowNumberAndColumName() {
        assertEquals("Number", readCSV.searchByRowNumberAndColumName(0, "Number"));
        assertEquals("Australia", readCSV.searchByRowNumberAndColumName(3, "Country"));

        String[] headers = {"Number", "Cod", "Country"};
        assertArrayEquals(headers, readCSV.getHeaders());
    }
}