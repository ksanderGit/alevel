package com.alevel.nix;

import com.alevel.nix.dao.*;
import com.alevel.nix.entity.*;
import com.alevel.nix.service.NodeService;
import com.alevel.nix.service.ReadingFromFile;
import com.alevel.nix.service.SolutionService;
import java.util.List;

public class RunApi {
    public static void main(String[] args) {
        FindRoutesDao findRoutesDao = new FindRoutesDao();
        SolutionService solutionService = new SolutionService();
        List<Node> node = ReadingFromFile.readFile(args[0]);
        ReadingFromFile.writeToDB(node);
        NodeService nodeService = new NodeService();
        List<Node> nodes = nodeService.createNodes();
        List<FoundRoute> listRoutes = solutionService.foundShortestRoutes(nodes);
        for (FoundRoute routes : listRoutes) {
            findRoutesDao.save(routes);
        }
    }
}
