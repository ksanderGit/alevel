package com.alevel.nix.dao;

import com.alevel.nix.entity.City;
import com.alevel.nix.util.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CityDao {
    public static List<City> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<City> criteria = builder.createQuery(City.class);
        criteria.from(City.class);
        List<City> cities = session.createQuery(criteria).getResultList();
        session.close();
        return cities;
    }

    public City find(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        City city = session.get(City.class, id);
        session.close();
        return city;
    }

    public void save(List<City> cities) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        for (City city : cities)
            session.save(city);
        session.close();
    }
}
