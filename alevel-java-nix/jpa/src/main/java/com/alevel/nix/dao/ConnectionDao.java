package com.alevel.nix.dao;

import com.alevel.nix.entity.Connection;
import com.alevel.nix.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ConnectionDao {
    public static List<Connection> getListConnections() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Connection> list = (List<Connection>)session.createSQLQuery("SELECT * FROM CONNECTION ").addEntity(Connection.class).list();
        session.close();
        return list;
    }

    public void save(List<Connection> connections) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        for (Connection connection : connections)
            session.save(connection);
        session.close();
    }
}
