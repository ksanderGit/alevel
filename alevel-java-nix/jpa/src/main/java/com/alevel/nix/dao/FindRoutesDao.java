package com.alevel.nix.dao;

import com.alevel.nix.entity.FoundRoute;
import com.alevel.nix.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class FindRoutesDao {
    public void save(FoundRoute found_routes) {
        Transaction tx = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            tx = session.beginTransaction();
            session.save(found_routes);
            tx.commit();
        }catch (Exception e) {
            tx.rollback();
            throw new RuntimeException(e);
        }
    }
}
