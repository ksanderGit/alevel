package com.alevel.nix.dao;

import com.alevel.nix.entity.Problem;
import com.alevel.nix.util.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class ProblemDao {
    public List<Problem> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Problem> criteria = builder.createQuery(Problem.class);
        criteria.from(Problem.class);
        List<Problem> problems = session.createQuery(criteria).getResultList();
        session.close();
        return problems;
    }

    public void save(List<Problem> problems) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        for (Problem problem : problems)
        session.save(problem);
        session.close();
    }
}
