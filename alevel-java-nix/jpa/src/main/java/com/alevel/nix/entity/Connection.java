package com.alevel.nix.entity;


import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Objects;

@Entity
@Table(name = "connection")
public class Connection {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fromCity")
    private City fromCity;

    public void setFromCity(City fromCity) {
        this.fromCity = fromCity;
    }

    public void setToCity(City toCity) {
        this.toCity = toCity;
    }

    @ManyToOne
    @JoinColumn(name = "toCity")
    private City toCity;

    @Column(name = "cost")
    private Integer cost = Integer.MAX_VALUE;

    public Long getId() {
        return this.id;
    }

    public City getFromCity() {
        return this.fromCity;
    }

    public City getToCity() {
        return this.toCity;
    }

    public Integer getCost() {
        return this.cost;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Connection that = (Connection) o;
        return id.equals(that.id) &&
                fromCity.equals(that.fromCity) &&
                toCity.equals(that.toCity) &&
                cost.equals(that.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fromCity, toCity, cost);
    }

    public String toString() {
        return "Connection(id=" + this.getId() + ", fromCity=" + this.getFromCity()
                + ", toCity=" + this.getToCity() + ", cost=" + this.getCost() + ")";
    }
}
