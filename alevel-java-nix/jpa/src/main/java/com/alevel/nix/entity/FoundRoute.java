package com.alevel.nix.entity;


import javax.persistence.*;

@Entity
@Table(name = "find_routes")
public class FoundRoute {
    @Id
    @Column(name = "problem_id", nullable = false)
    private Long id;

    @Column(name = "min_cost")
    private Integer cost;

    public FoundRoute() {
    }

    public FoundRoute(Long id, Integer cost) {
        this.id = id;
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
