package com.alevel.nix.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Instant;
@Embeddable
public class TimeStamps {
    @Column(name = "created_at", nullable = false)
    private Instant createAt;
    @Column(name = "modified_at", nullable = false)
    private Instant modifiedAt;

    public TimeStamps() {
    }

    public TimeStamps(Instant createAt, Instant modifiedAt) {
        this.createAt = createAt;
        this.modifiedAt = modifiedAt;
    }

    public static TimeStamps now(){
        var now = Instant.now();
        return new TimeStamps(now, now);
    }
}
