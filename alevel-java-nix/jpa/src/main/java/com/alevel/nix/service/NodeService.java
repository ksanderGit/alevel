package com.alevel.nix.service;

import com.alevel.nix.dao.CityDao;
import com.alevel.nix.dao.ConnectionDao;
import com.alevel.nix.entity.City;
import com.alevel.nix.entity.Connection;
import com.alevel.nix.entity.Node;

import java.util.ArrayList;
import java.util.List;

public class NodeService {
    private List<Node> nodes = new ArrayList<>();

    public List<Node> createNodes() {
        for (Connection connect : ConnectionDao.getListConnections()) {
            nodes.get(connect.getFromCity().getId() - 1).addDestination(nodes.get(connect.getToCity().getId()-1), connect.getCost());
        }
        return nodes;
    }

    public NodeService() {
        nodes = createListNodes();
    }

    private List<Node> createListNodes() {
        for (City city : CityDao.findAll()){
            Node node = new Node(city.getName(), city.getId());
            nodes.add(node);
        }
        return nodes;
    }

}
