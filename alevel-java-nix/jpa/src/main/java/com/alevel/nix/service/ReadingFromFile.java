package com.alevel.nix.service;

import com.alevel.nix.dao.CityDao;
import com.alevel.nix.dao.ConnectionDao;
import com.alevel.nix.dao.ProblemDao;
import com.alevel.nix.entity.City;
import com.alevel.nix.entity.Connection;
import com.alevel.nix.entity.Node;
import com.alevel.nix.entity.Problem;

import java.io.*;
import java.util.*;

public class ReadingFromFile {
    public  static List<Node> readFile (String path) {
        File file = new File(path);
        List<Node> nodes;
        Queue<String> listNeighbors = new ArrayDeque<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            nodes = new ArrayList<>(Integer.parseInt(reader.readLine()));
            String result;
            int id = 1;
            while ((result = reader.readLine()) != null) {
                Node node = new Node(result, id);
                nodes.add(node);
                int neighbors = Integer.parseInt(reader.readLine());
                node.setNeighbors(neighbors);
                while (neighbors > 0) {
                    listNeighbors.add(reader.readLine());
                    neighbors--;
                }
                id++;
            }
        }catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        for (Node node : nodes) {
            int negihbors = node.getNeighbors();
            while (negihbors > 0) {
                String[] s = listNeighbors.remove().split(" ");
                node.addDestination(nodes.get(Integer.parseInt(s[0]) - 1), Integer.parseInt(s[1]));
                negihbors--;
            }
        }
        return nodes;
    }

    public static void writeToDB(List<Node> nodes) {
        List<City> cities = new ArrayList<>();
        List<Connection> connections = new ArrayList<>();
        List<Problem> problems = new ArrayList<>();
        for (Node node : nodes) {
            City city = new City();
            city.setName(node.getName());
            cities.add(city);
        }
        CityDao cityDao = new CityDao();
        cityDao.save(cities);
        for (Node node : nodes) {
            City city = cityDao.find(node.getId());
            for (Map.Entry<Node, Integer> pair : node.getAdjacentNodes().entrySet()) {
                Connection connection = new Connection();
                connection.setFromCity(city);
                connection.setToCity(cityDao.find(pair.getKey().getId()));
                connection.setCost(pair.getValue());
                connections.add(connection);
            }
        }
        ConnectionDao connectionDao = new ConnectionDao();
        connectionDao.save(connections);
        for (City city : cities) {
            int i = city.getId();
            for (; i < cities.size(); i++) {
                Problem problem = new Problem();
                problem.setFromCity(city);
                problem.setToCity(cities.get(i));
                problems.add(problem);
            }
        }

        ProblemDao problemDao = new ProblemDao();
        problemDao.save(problems);
    }
}
