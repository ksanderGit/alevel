package com.alevel.nix.service;

import com.alevel.nix.dao.ProblemDao;
import com.alevel.nix.entity.FoundRoute;
import com.alevel.nix.entity.Node;
import com.alevel.nix.entity.Problem;

import java.util.ArrayList;
import java.util.List;

public class SolutionService {
    private ProblemDao problemDao = new ProblemDao();
    public List<FoundRoute> foundShortestRoutes(List<Node> nodes) {
        List<FoundRoute> routes = new ArrayList<>();
        for (Problem p : problemDao.findAll()) {
            int from = p.getFromCity().getId() - 1;
            int to = p.getToCity().getId() - 1;
            Node n = OptimalWay.calculateShortestPathFromSource(nodes.get(to), nodes.get(from));
            FoundRoute route = new FoundRoute(p.getId(), n.getDistance());
            routes.add(route);
        }
        return routes;
    }
}
