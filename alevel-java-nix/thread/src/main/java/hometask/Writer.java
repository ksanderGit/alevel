package hometask;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class Writer {
    private StringBuilder stringBuilder = new StringBuilder();
    private boolean isAlive = true;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    class WriteToFile implements Runnable {
        private static final String FILE_NAME = "output.txt";

        @Override
        public void run() {
            String result = "";
            try (FileWriter writer = new FileWriter(FILE_NAME, false)) {
                while (isAlive) {
                    String temp = stringBuilder.toString();
                    if (!temp.equals(result)) {
                        result = temp;
                        writer.write(result);
                        writer.append('\n');
                        writer.flush();
                        Thread.sleep(1000);
                    }
                }
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void readConsole() {
        String temp;
        Thread thread = new Thread(new WriteToFile());
        thread.start();
        try {
           while (true) {
               temp = reader.readLine();
               if (temp.equals("quit")){
                   isAlive = false;
                   return;
               }else {
                   stringBuilder.append(temp);
               }
           }
        }catch (IOException e)  {
            e.printStackTrace();
        }
    }
    public static void main(String[] args)  {
        Writer writer = new Writer();
        writer.readConsole();
    }
}
