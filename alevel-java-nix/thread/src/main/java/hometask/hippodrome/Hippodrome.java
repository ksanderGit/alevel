package hometask.hippodrome;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

public class Hippodrome {
    private List<Horse> horses;
    private String nameHorse;

    public Hippodrome(List<Horse> h) {
        horses = h;
    }

    public void move() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(horses.size());
        CompletionService<String> service = new ExecutorCompletionService<>(executor);
        for (Horse h : horses) {
           service.submit(h);
        }
        executor.shutdown();
        int i = 0;
        int position = 0;
        while (!executor.isTerminated()) {
            Future<String> future = service.take();
            i++;
            System.out.println("Horse name " + future.get() + " has finished");
            if(future.get().equals(nameHorse)) {
                position = i;
            }
        }
        System.out.println(position);
        System.out.println("All finished");
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Horse> hrss = new ArrayList<>();
        for (int i = 1; i < 20; i++) {
            hrss.add(new Horse("horse" + i, 2));
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input horse number from 1 to " + (hrss.size()+1));
        int number = Integer.parseInt(scanner.next())-1;
        Hippodrome hippodrome = new Hippodrome(hrss);
        hippodrome.nameHorse = hrss.get(number).getName();
        hippodrome.move();
    }
}
