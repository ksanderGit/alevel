package hometask.hippodrome;

import java.util.concurrent.Callable;

public class Horse implements Callable<String> {
    private String name;
    private int distance = 1000;
    private double speed;

    public Horse(String name, double speed) {
        this.name = name;
        this.speed = speed;
    }

    @Override
    public String call() {
        System.out.println("Horse number " + name + " started in thread: " + Thread.currentThread().getName());
        while (distance > 0) {
            try {
                move();
                Thread.sleep((int)(Math.random() * 100 + 100));
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
        }
        System.out.println(name + " is finished");
        return name;
    }

    public void move() {
        double segment = speed * Math.random();
        if (segment > 0.5) {
            distance = distance - (int) (segment * 100);
        }else {
            distance = distance - (int) (segment * 100 + 100);
        }

    }

    public String getName() {
        return name;
    }
}
