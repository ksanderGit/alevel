package com.alevel.nix.market_computer_parts_with_overviews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketComputerPartsWithOverviewsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketComputerPartsWithOverviewsApplication.class, args);
    }

}
