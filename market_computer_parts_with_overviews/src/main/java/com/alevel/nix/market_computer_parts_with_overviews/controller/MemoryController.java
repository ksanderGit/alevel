package com.alevel.nix.market_computer_parts_with_overviews.controller;
import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveMemoryRequest;
import com.alevel.nix.market_computer_parts_with_overviews.entity.response.MemoryResponse;
import com.alevel.nix.market_computer_parts_with_overviews.service.MemoryOPS;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/memory")
public class MemoryController {
    private final MemoryOPS memoryOPS;

    public MemoryController(MemoryOPS memoryOPS) {
        this.memoryOPS = memoryOPS;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<MemoryResponse> create(@RequestBody SaveMemoryRequest request) {
        var saveMemoryRequest = request.save();
        var memory = memoryOPS.create(saveMemoryRequest);

        return ResponseEntity
                .created(URI.create("/memory/" + memory.getId()))
                .body(MemoryResponse.fromMemory(memory));
    }

    @GetMapping("/{id}")
    public MemoryResponse getById(@PathVariable Long id) {
        var memory = memoryOPS.getById(id);
        return MemoryResponse.fromMemory(memory);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Long id, @RequestBody SaveMemoryRequest request) {
        memoryOPS.update(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@PathVariable Long id) {
        memoryOPS.deleteById(id);
    }

    @GetMapping
    public void getListMemory(Model model) {
        model.addAttribute("memorylist", memoryOPS.getAllMemory());
    }
}
