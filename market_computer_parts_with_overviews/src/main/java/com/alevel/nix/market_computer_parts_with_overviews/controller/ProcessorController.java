package com.alevel.nix.market_computer_parts_with_overviews.controller;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Processor;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveProcessorRequest;
import com.alevel.nix.market_computer_parts_with_overviews.service.ProcessorOPS;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/processor")
public class ProcessorController {
    private final ProcessorOPS processorOPS;

    public ProcessorController(ProcessorOPS processorOPS) {
        this.processorOPS = processorOPS;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Processor create(@RequestBody SaveProcessorRequest request) {
        var saveProcessorRequest = request.saveProcessorRequest();
        return processorOPS.create(saveProcessorRequest);
    }

    @GetMapping("/{id}")
    public Processor getById(@PathVariable Long id) {
        return processorOPS.getById(id);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Long id, @RequestBody SaveProcessorRequest request) {
        processorOPS.update(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@PathVariable Long id) {
       processorOPS.deleteById(id);
    }

    @GetMapping
    public List<Processor> getListProcessors() {
        return processorOPS.getAllProcessor();
    }
}
