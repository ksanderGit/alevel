package com.alevel.nix.market_computer_parts_with_overviews.controller;

import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveUserRequest;
import com.alevel.nix.market_computer_parts_with_overviews.entity.response.UserResponse;
import com.alevel.nix.market_computer_parts_with_overviews.exception.UserNotFoundException;
import com.alevel.nix.market_computer_parts_with_overviews.service.UserOPS;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
public class UserController {
    private final UserOPS userOperation;

    public UserController(UserOPS userOperation) {
        this.userOperation = userOperation;
    }

    @GetMapping("/users/{id}")
    public UserResponse getById(@PathVariable Long id) throws UserNotFoundException {
        var user = userOperation.getById(id);
        if (user == null) {
            throw new UserNotFoundException(id);
        }
        return UserResponse.fromUser(user);
    }

    @PostMapping
    @RequestMapping("/registration")
    public ResponseEntity<UserResponse> create(@RequestBody SaveUserRequest request) {
        var saveRequest = request.toSaveUserRequest();

        var user = userOperation.create(saveRequest);

        return ResponseEntity
                .created(URI.create("/users/" + user.getId()))
                .body(UserResponse.fromUser(user));
    }

    @PutMapping("/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Long id, @RequestBody SaveUserRequest request) {
        userOperation.update(id, request.toSaveUserRequest());
    }

    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        userOperation.deleteById(id);
    }
}
