package com.alevel.nix.market_computer_parts_with_overviews.entity.request;

public class SaveMemoryRequest {
    private String title;
    private String type;
    private String typeMemory;
    private Integer capacity;
    private Double price;
    private String overview;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeMemory() {
        return typeMemory;
    }

    public void setTypeMemory(String typeMemory) {
        this.typeMemory = typeMemory;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public SaveMemoryRequest save() {
        var saveMemoryRequest = new SaveMemoryRequest();
        saveMemoryRequest.setCapacity(capacity);
        saveMemoryRequest.setOverview(overview);
        saveMemoryRequest.setPrice(price);
        saveMemoryRequest.setTitle(title);
        saveMemoryRequest.setType(type);
        saveMemoryRequest.setTypeMemory(typeMemory);
        return saveMemoryRequest;
    }
}
