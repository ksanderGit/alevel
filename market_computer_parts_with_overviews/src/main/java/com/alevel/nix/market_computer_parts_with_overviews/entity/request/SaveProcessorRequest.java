package com.alevel.nix.market_computer_parts_with_overviews.entity.request;

public class SaveProcessorRequest {
    private String title;
    private String socket;
    private Double frequency;
    private Integer cores;
    private Integer cache;
    private Double price;
    private String overview;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public Double getFrequency() {
        return frequency;
    }

    public void setFrequency(Double frequency) {
        this.frequency = frequency;
    }

    public Integer getCores() {
        return cores;
    }

    public void setCores(Integer cores) {
        this.cores = cores;
    }

    public Integer getCache() {
        return cache;
    }

    public void setCache(Integer cache) {
        this.cache = cache;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public SaveProcessorRequest saveProcessorRequest() {
        var processorRequest = new SaveProcessorRequest();
        processorRequest.setTitle(title);
        processorRequest.setCache(cache);
        processorRequest.setCores(cores);
        processorRequest.setFrequency(frequency);
        processorRequest.setOverview(overview);
        processorRequest.setPrice(price);
        processorRequest.setSocket(socket);
        return processorRequest;
    }
}
