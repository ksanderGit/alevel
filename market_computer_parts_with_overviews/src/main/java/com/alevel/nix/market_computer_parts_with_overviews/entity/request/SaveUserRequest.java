package com.alevel.nix.market_computer_parts_with_overviews.entity.request;

import java.util.Collections;
import java.util.Set;

public class SaveUserRequest {
    public static final String REGULAR_USER_ROLE_NAME = "USER";

    private String username;

    private String password;

    private Set<String> roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public SaveUserRequest toSaveUserRequest() {
        var saveRequest = new SaveUserRequest();
        saveRequest.setUsername(username);
        saveRequest.setPassword(password);
        saveRequest.setRoles(Collections.singleton(REGULAR_USER_ROLE_NAME));
        return saveRequest;
    }

}
