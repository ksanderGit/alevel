package com.alevel.nix.market_computer_parts_with_overviews.entity.response;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;

import javax.persistence.Column;
import javax.validation.constraints.Min;

public class MemoryResponse {
    private Long id;
    private String title;
    private String type;
    private String typeMemory;
    private Integer capacity;
    private String overview;

    public MemoryResponse() {
    }

    public MemoryResponse(Long id, String title, String type, String typeMemory, Integer capacity, String overview) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.typeMemory = typeMemory;
        this.capacity = capacity;
        this.overview = overview;
    }

    public static MemoryResponse fromMemory(Memory memory) {
        return new MemoryResponse(
                memory.getId(),
                memory.getTitle(),
                memory.getType(),
                memory.getTypeMemory(),
                memory.getCapacity(),
                memory.getOverview()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeMemory() {
        return typeMemory;
    }

    public void setTypeMemory(String typeMemory) {
        this.typeMemory = typeMemory;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
