package com.alevel.nix.market_computer_parts_with_overviews.entity.response;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Role;
import com.alevel.nix.market_computer_parts_with_overviews.entity.User;

import java.util.Set;
import java.util.stream.Collectors;

public class UserResponse {
    private Long id;

    private String username;

    private Set<String> roles;

    public UserResponse(Long id, String username, Set<String> roles) {
        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public UserResponse() {
    }

    public static UserResponse fromUser(User user) {
        return new UserResponse(
                user.getId(),
                user.getUsername(),
                user.getRoles().stream().map(Role::getName).collect(Collectors.toSet())
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
