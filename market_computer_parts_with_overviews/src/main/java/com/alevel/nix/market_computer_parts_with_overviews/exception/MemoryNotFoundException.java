package com.alevel.nix.market_computer_parts_with_overviews.exception;

import org.springframework.http.HttpStatus;

public class MemoryNotFoundException extends ResponseException {
    public MemoryNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, "Memory with id " + id + " not found");
    }

    public MemoryNotFoundException(String title) {
        super(HttpStatus.NOT_FOUND, "Memory with title " + title + " not found");
    }
}
