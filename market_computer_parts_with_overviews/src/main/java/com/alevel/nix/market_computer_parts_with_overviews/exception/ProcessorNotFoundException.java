package com.alevel.nix.market_computer_parts_with_overviews.exception;

import org.springframework.http.HttpStatus;

public class ProcessorNotFoundException extends ResponseException {
    public ProcessorNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, "Processor with id " + id + " not found");
    }
}
