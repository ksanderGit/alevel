package com.alevel.nix.market_computer_parts_with_overviews.exception;

import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends ResponseException {
    public RoleNotFoundException(String name) {
        super(HttpStatus.NOT_FOUND, "Role " + name + "  not found");
    }
}
