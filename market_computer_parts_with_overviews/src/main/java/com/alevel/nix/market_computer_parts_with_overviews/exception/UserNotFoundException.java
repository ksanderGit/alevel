package com.alevel.nix.market_computer_parts_with_overviews.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UserNotFoundException extends ResponseException {
    public UserNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, "User with id " + id + "  not found");
    }

    public UserNotFoundException(String username) {
        super(HttpStatus.NOT_FOUND, "User " + username + "  not found");
    }
}
