package com.alevel.nix.market_computer_parts_with_overviews.exception;

import org.springframework.http.HttpStatus;

public class UsernameAlreadyExistException extends ResponseException {
    public UsernameAlreadyExistException(String name) {
        super(HttpStatus.BAD_REQUEST, "User " + name + " already exists");
    }
}
