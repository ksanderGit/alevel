package com.alevel.nix.market_computer_parts_with_overviews.repo;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemoryRepo extends JpaRepository<Memory, Long> {

}
