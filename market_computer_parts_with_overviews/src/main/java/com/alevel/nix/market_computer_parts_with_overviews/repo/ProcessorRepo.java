package com.alevel.nix.market_computer_parts_with_overviews.repo;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Processor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessorRepo extends JpaRepository<Processor, Long> {
}
