package com.alevel.nix.market_computer_parts_with_overviews.repo;

import com.alevel.nix.market_computer_parts_with_overviews.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String userName);

    boolean existsByUsername(String userName);

}

