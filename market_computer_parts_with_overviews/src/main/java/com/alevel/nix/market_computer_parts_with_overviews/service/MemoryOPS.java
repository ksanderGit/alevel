package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveMemoryRequest;

import java.util.List;

public interface MemoryOPS {
    Memory create(SaveMemoryRequest request);

    void update(Long id, SaveMemoryRequest request);

    Memory getById(Long id);

    void deleteById(Long id);

    List<Memory> getAllMemory();

}
