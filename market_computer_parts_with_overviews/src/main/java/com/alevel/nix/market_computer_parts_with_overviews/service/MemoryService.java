package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveMemoryRequest;
import com.alevel.nix.market_computer_parts_with_overviews.exception.MemoryNotFoundException;
import com.alevel.nix.market_computer_parts_with_overviews.repo.MemoryRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class MemoryService implements MemoryOPS {
    private final MemoryRepo memoryRepo;

    public MemoryService(MemoryRepo memoryRepo) {
        this.memoryRepo = memoryRepo;
    }

    @Override
    public Memory create(SaveMemoryRequest request) {
        var memory = new Memory();
        memory.setTitle(request.getTitle());
        memory.setCapacity(request.getCapacity());
        memory.setType(request.getType());
        memory.setTypeMemory(request.getTypeMemory());
        memory.setOverview(request.getOverview());
        memory.setPrice(request.getPrice());
        return memoryRepo.save(memory);
    }

    @Override
    public void update(Long id, SaveMemoryRequest request) throws MemoryNotFoundException {
        var exsistingMemory = memoryRepo.findById(id).orElseThrow(() -> new MemoryNotFoundException(id));
        exsistingMemory.setPrice(request.getPrice());
        exsistingMemory.setOverview(request.getOverview());
        exsistingMemory.setTypeMemory(request.getTypeMemory());
        exsistingMemory.setCapacity(request.getCapacity());
        exsistingMemory.setType(request.getType());
        exsistingMemory.setPrice(request.getPrice());
        exsistingMemory.setTitle(request.getTitle());
        memoryRepo.save(exsistingMemory);
    }

    @Override
    public Memory getById(Long id) throws MemoryNotFoundException {
        return memoryRepo.findById(id).orElseThrow(() -> new MemoryNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        memoryRepo.deleteById(id);
    }

    @Override
    public List<Memory> getAllMemory() {
        return memoryRepo.findAll();
    }
}
