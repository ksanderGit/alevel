package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Memory;
import com.alevel.nix.market_computer_parts_with_overviews.entity.Processor;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveMemoryRequest;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveProcessorRequest;

import java.util.List;

public interface ProcessorOPS {
    Processor create(SaveProcessorRequest request);

    void update(Long id, SaveProcessorRequest request);

    Processor getById(Long id);

    void deleteById(Long id);

    List<Processor> getAllProcessor();
}
