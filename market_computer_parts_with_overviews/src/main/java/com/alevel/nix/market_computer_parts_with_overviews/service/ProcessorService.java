package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Processor;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveProcessorRequest;
import com.alevel.nix.market_computer_parts_with_overviews.exception.ProcessorNotFoundException;
import com.alevel.nix.market_computer_parts_with_overviews.repo.ProcessorRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class ProcessorService implements ProcessorOPS {
    private final ProcessorRepo processorRepository;

    public ProcessorService(ProcessorRepo processorRepository) {
        this.processorRepository = processorRepository;
    }

    @Override
    public Processor create(SaveProcessorRequest request) {
        var processor = new Processor();
        processor.setCache(request.getCache());
        processor.setCores(request.getCores());
        processor.setFrequency(request.getFrequency());
        processor.setOverview(request.getOverview());
        processor.setSocket(request.getSocket());
        processor.setPrice(request.getPrice());
        processor.setTitle(request.getTitle());
        return processorRepository.save(processor);
    }

    @Override
    public void update(Long id, SaveProcessorRequest request) throws ProcessorNotFoundException {
        var existingProcessor = processorRepository.findById(id).orElseThrow(() -> new ProcessorNotFoundException(id));
        existingProcessor.setTitle(request.getTitle());
        existingProcessor.setPrice(request.getPrice());
        existingProcessor.setSocket(request.getSocket());
        existingProcessor.setOverview(request.getOverview());
        existingProcessor.setFrequency(request.getFrequency());
        existingProcessor.setCores(request.getCores());
        existingProcessor.setCache(request.getCache());
        processorRepository.save(existingProcessor);
    }

    @Override
    public Processor getById(Long id) throws ProcessorNotFoundException{
        return processorRepository.findById(id).orElseThrow(() -> new ProcessorNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        processorRepository.deleteById(id);
    }

    @Override
    public List<Processor> getAllProcessor() {
        return processorRepository.findAll();
    }
}
