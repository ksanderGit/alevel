package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.User;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveUserRequest;

public interface UserOPS {
    User create(SaveUserRequest request);

    void update(Long id, SaveUserRequest request);

    User getById(Long id);

    void deleteById(Long id);
}
