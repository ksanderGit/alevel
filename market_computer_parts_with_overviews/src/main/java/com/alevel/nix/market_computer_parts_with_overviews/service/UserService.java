package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.entity.Role;
import com.alevel.nix.market_computer_parts_with_overviews.entity.User;
import com.alevel.nix.market_computer_parts_with_overviews.entity.request.SaveUserRequest;
import com.alevel.nix.market_computer_parts_with_overviews.exception.RoleNotFoundException;
import com.alevel.nix.market_computer_parts_with_overviews.exception.UserNotFoundException;
import com.alevel.nix.market_computer_parts_with_overviews.exception.UsernameAlreadyExistException;
import com.alevel.nix.market_computer_parts_with_overviews.repo.RoleRepo;
import com.alevel.nix.market_computer_parts_with_overviews.repo.UserRepo;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class UserService implements UserOPS {

    private final UserRepo userRepository;

    private final RoleRepo roleRepository;

    private final PasswordEncoder passwordEncoder;


    public UserService(UserRepo userRepository, RoleRepo roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(SaveUserRequest request) {
        String username = request.getUsername();

        if (userRepository.existsByUsername(username)) {
            throw new UsernameAlreadyExistException(username);
        }

        Set<Role> roles = getRolesByNames(request.getRoles());

        var user = new User();
        user.setUsername(username);
        user.setPassword(request.getPassword());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);

        return userRepository.save(user);

    }

    @Override
    public void update(Long id, SaveUserRequest request) {
        var existingUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        String username = request.getUsername();

        if (!existingUser.getUsername().equals(username) && userRepository.existsByUsername(username)) {
            throw new UsernameAlreadyExistException(username);
        }

        Set<Role> roles = getRolesByNames(request.getRoles());

        existingUser.setUsername(username);
        existingUser.setPassword(passwordEncoder.encode(request.getPassword()));
        existingUser.setRoles(roles);

        userRepository.save(existingUser);
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    private Set<Role> getRolesByNames(Set<String> roleNames) {
        Set<Role> roles = new HashSet<>(roleNames.size());

        for (String roleName : roleNames) {
            var role = roleRepository
                    .findByName(roleName)
                    .orElseThrow(() -> new RoleNotFoundException(roleName));
            roles.add(role);
        }
        return roles;
    }

}
