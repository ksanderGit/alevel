package com.alevel.nix.market_computer_parts_with_overviews.service;

import com.alevel.nix.market_computer_parts_with_overviews.repo.UserRepo;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
@Transactional
public class UsrDetailsService implements UserDetailsService {
    private static final String ROLE_PREFIX = "ROLE_";

    private final UserRepo userRepository;

    public UsrDetailsService(UserRepo userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        var user = userRepository
                .findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException("User " + userName + " not found"));

        return new User(
                user.getUsername(),
                user.getPassword(),
                user.getRoles().stream()
                        .map(role -> new SimpleGrantedAuthority(ROLE_PREFIX + role.getName()))
                        .collect(Collectors.toList())
        );
    }

}
