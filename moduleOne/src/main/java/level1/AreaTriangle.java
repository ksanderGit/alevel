package level1;

public class AreaTriangle {
    public double area (Point a, Point b, Point c) {
       if (a == null || b == null || c == null) {
           return 0.0;
       }else if (a.equals(b) || a.equals(c) || b.equals(c)) {
           return 0.0;
       }
    double area = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2.0;
    return Math.abs(area);
    }
}
