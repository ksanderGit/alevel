package level1;

public class ChessBoard {
    public boolean checkMove(int x, int y, int moveX, int moveY) {
        int dx = Math.abs(x - moveX);
        int dy = Math.abs(y - moveY);
        if (dx == 1 & dy == 2 || dx == 2 & dy == 1) {
            return true;
        }else {
            return false;
        }
    }
}
