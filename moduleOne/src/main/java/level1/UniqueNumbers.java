package level1;

import java.util.HashSet;
import java.util.Set;

public class UniqueNumbers {
    public int uniqueNum (int[] array) {
        if (array.length < 1) {
           return array.length;
        }

        Set<Integer> uniqueNumbers = new HashSet<Integer>();

        for (int i = 0, length = array.length; i < length; i++) {
            uniqueNumbers.add(array[i]);
        }
        return uniqueNumbers.size();
    }
}
