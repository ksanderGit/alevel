package level2;

import java.util.Stack;

public class CheckString {

    public boolean isAvailableStr(String input) {
        Stack<Character> stack = new Stack<>();
        if (input == null) {
            return false;
        }
        if (input.length() < 1) {
            return false;
        }

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                if (stack.isEmpty()) {
                   return false;
                }
                if (stack.peek() == '(') {
                    stack.pop();
                }
            }

            if (c == '[') {
                stack.push(c);
            } else if (c == ']') {
               if (stack.isEmpty()) {
                   return false;
               }
                if (stack.peek() == '[') {
                    stack.pop();
                }
            }

            if (c == '{') {
                stack.push(c);
            } else if (c == '}') {
               if (stack.isEmpty()) {
                   return false;
               }
                if (stack.peek() == '{') {
                    stack.pop();
                }
            }
        }
        return stack.isEmpty();
}
}
