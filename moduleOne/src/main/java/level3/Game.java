package level3;

public class Game {

    public int[][] nextGeneration(int grid[][])
    {
        int M = grid.length;
        int N = grid[0].length;

        if (M < 3 || N < 3) {
            return new int[3][3];
        }
        //**********************
//        System.out.println("Current Generation");
//        for (int i = 0; i < M; i++)
//        {
//            for (int j = 0; j < N; j++)
//            {
//                if (grid[i][j] == 0)
//                    System.out.print("⥁");
//                else
//                    System.out.print("✭");
//            }
//            System.out.println();
//        }
//        System.out.println();
        //************************
        int[][] newGrid = new int[M][N];


        for (int row = 1; row < M - 1; row++)
        {
            for (int colum = 1; colum < N - 1; colum++)
            {

                int aliveNeighbours = 0;
                for (int i = -1; i <= 1; i++)
                    for (int j = -1; j <= 1; j++)
                        aliveNeighbours += grid[row + i][colum + j];


                aliveNeighbours -= grid[row][colum];

                // implementing the rules of Life

                if ((grid[row][colum] == 1) && (aliveNeighbours < 2)) {
                    newGrid[row][colum] = 0;
                }
                else if ((grid[row][colum] == 1) && (aliveNeighbours > 3)) {
                    newGrid[row][colum] = 0;
                }
                else if ((grid[row][colum] == 0) && (aliveNeighbours == 3)) {
                    newGrid[row][colum] = 1;
                }
                else
                    newGrid[row][colum] = grid[row][colum];
            }
        }
        //************************
//        System.out.println("Next Generation");
//        for (int i = 0; i < M; i++)
//        {
//            for (int j = 0; j < N; j++)
//            {
//                if (newGrid[i][j] == 0)
//                    System.out.print("⥁");
//                else
//                    System.out.print("✭");
//            }
//            System.out.println();
//        }
        //*************************
        return newGrid;
    }
}
