package level1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AreaTriangleTest {
    AreaTriangle area;
    Point A;
    Point B;
    Point C;
    @BeforeEach
    void setUp() {
        area = new AreaTriangle();
        A = createPoint(13, 34);
        B = createPoint(22, 21);
        C = createPoint(11, 19);
    }

    @Test
    void area() {
        double expected = 80.5;
        double actual = area.area(A, B, C);
        assertEquals(expected, actual);
        A = createPoint(10, 10);
        B = createPoint(5,5);
        C = createPoint(10,10);
        actual = area.area(A, B, C);
        assertEquals(0.0, actual);
        A = createPoint(3,3);
        B = null;
        C = createPoint(2,7);
        actual = area.area(A, B, C);
        assertEquals(0.0, actual);
    }

    public Point createPoint (int x, int y) {
       return new Point(x, y);
    }
}