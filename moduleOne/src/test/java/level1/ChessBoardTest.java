package level1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessBoardTest {
    ChessBoard chess;
    @BeforeEach
    void setUp() {
        chess = new ChessBoard();
    }

    @Test
    void checkMove() {
        boolean expected = true;
        boolean actual = chess.checkMove(1,1, 0, -1);
        assertEquals(expected, actual);
        assertEquals(false, chess.checkMove(1,1, 2, 2));
    }
}