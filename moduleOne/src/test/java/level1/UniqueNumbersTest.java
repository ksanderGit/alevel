package level1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueNumbersTest {
    UniqueNumbers uniqueNumbers;
    @BeforeEach
    void setUp() {
        uniqueNumbers = new UniqueNumbers();
    }

    @Test
    void uniqueNum() {
    int expected = 4;
    int actual = uniqueNumbers.uniqueNum(new int[]{1,2,3,4,1,2,3,2});
    assertEquals(expected, actual);
    assertEquals(1, uniqueNumbers.uniqueNum(new int[]{0}));
    }
}