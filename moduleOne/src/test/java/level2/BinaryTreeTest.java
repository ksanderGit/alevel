package level2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeTest {
    BinaryTree binaryTree;
    @BeforeEach
    void setUp() {
        binaryTree = new BinaryTree();
        binaryTree.root = new Node(1);
        binaryTree.root.left = new Node(2);
        binaryTree.root.right = new Node(3);
        binaryTree.root.left.left = new Node(4);
    }

    @Test
    void maxDepth() {
        int expected = 3;
        int actual = binaryTree.maxDepth(binaryTree.root);
        assertEquals(expected, actual);
        assertEquals(0, binaryTree.maxDepth(null));
    }

}