package level2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckStringTest {
    CheckString check;
    @BeforeEach
    void setUp() {
      check = new CheckString();
    }

    @Test
    void isAvailableStr() {
        testAvailableStr(true, check.isAvailableStr("[(){}]"));
        testAvailableStr(false, check.isAvailableStr("[]]["));
        testAvailableStr(false, check.isAvailableStr("({[]}))[}"));
        testAvailableStr(true, check.isAvailableStr("(())"));
        testAvailableStr(false, check.isAvailableStr(")(()"));
        testAvailableStr(false, check.isAvailableStr("(()))("));
        testAvailableStr(true, check.isAvailableStr("{}"));
        testAvailableStr(true, check.isAvailableStr("[[]]"));
        testAvailableStr(false, check.isAvailableStr("([)"));
        testAvailableStr(true, check.isAvailableStr("({[{([])}()]})"));
    }
    void testAvailableStr(boolean expected, boolean actual) {
        assertEquals(expected, actual);
    }
}