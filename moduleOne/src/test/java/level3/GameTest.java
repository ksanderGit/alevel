package level3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    Game game;
    @BeforeEach
    void setUp() {
        game = new Game();
    }

    @Test
    void nextGeneration() {
        int[][] grid = {
                { 0, 1, 0, 0, 1, 0, 0},
                { 0, 0, 0, 1, 0, 0, 0},
                { 0, 1, 0, 0, 0, 0, 0},
                { 0, 0, 0, 1, 0, 0, 0},
                { 0, 1, 0, 0, 0, 0, 0},
                { 1, 0, 1, 0, 1, 1, 0},
                { 0, 1, 0, 0, 0, 0, 0}
        };
       int[][] expected = {
                { 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0},
                { 0, 1, 1, 1, 1, 0, 0},
                { 0, 0, 1, 0, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0}
        };
       int[][] gridNext = new int[3][3];
       int[][] actual = game.nextGeneration(grid);
       testNextGeneration(expected, actual);
       testNextGeneration(gridNext, game.nextGeneration(new int[2][2]));
       testNextGeneration(gridNext, game.nextGeneration(new int[1][0]));
    }

    void testNextGeneration(int[][] expected, int[][] actual) {
        assertArrayEquals(expected, actual);
    }
}