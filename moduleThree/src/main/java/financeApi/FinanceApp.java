package financeApi;

import financeApi.controller.AppController;
import financeApi.dao.AccountDao;
import financeApi.dao.CategoryDao;
import financeApi.dao.OperationDao;
import financeApi.dao.UserDao;
import financeApi.entities.Account;
import financeApi.entities.Category;
import financeApi.entities.Operation;
import financeApi.entities.User;
import financeApi.service.OperationByJDBC;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.SQLException;

public class FinanceApp {
    public static void main(String[] args) {
        String user = args[0];
        String password = args[1];
        String url = args[2];

        Configuration cfg = new Configuration();
        if (!url.isEmpty() && !user.isEmpty() && !password.isEmpty()) {
            cfg.setProperty("hibernate.connection.url", url)
                    .setProperty("hibernate.connection.username", user)
                    .setProperty("hibernate.connection.password", password)

                    .setProperty("hibernate.hbm2ddl.auto", "update")
                    .setProperty("dialect", "org.hibernate.dialect.PostgreSQL10Dialect")
                    .setProperty("hibernate.connection.driver_class",
                            "org.postgresql.Driver")

                    .addAnnotatedClass(Account.class)
                    .addAnnotatedClass(Category.class)
                    .addAnnotatedClass(Operation.class)
                    .addAnnotatedClass(User.class);
        } else {
            System.err.println("Please enter the arguments for the database to run the application");
            System.exit(1);
        }

        try (SessionFactory sf = cfg.buildSessionFactory();
             Session session = sf.openSession()
        ) {
            UserDao userDao = new UserDao(session);
            AccountDao accountDao = new AccountDao(session);
            CategoryDao categoryDao = new CategoryDao(session);
            OperationDao operationDao = new OperationDao(session);
            OperationByJDBC operationByJDBC = new OperationByJDBC(url, user, password);

            AppController controller = new AppController(userDao, accountDao, categoryDao, operationDao, operationByJDBC);
            controller.loop();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
