package financeApi.controller;

import financeApi.dao.AccountDao;
import financeApi.entities.User;
import financeApi.view.AccountView;
import financeApi.view.ControllerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class AccountController implements Controller {
        private static final Logger log = LoggerFactory.getLogger(AccountController.class);
        private final AccountView accountView;
        private final ControllerView controllerView;
        private final AccountDao accountDao;
        private Boolean isRunning = true;

        public AccountController(AccountDao accountDao) {
            accountView = new AccountView();
            controllerView = new ControllerView();
            this.accountDao = accountDao;
        }

    @Override
    public void loop(User user) {
        log.info("User {} launched account controller", user.getId());
        try {
            while (isRunning) {
                accountView.printLaunch();
                String value = reader.readLine();
                switch (value) {
                    case "-lists": {
                        log.info("List accounts for user {}", user.getId());
                        accountView.printAccounts(user.getAccountList());
                        break;
                    }
                    case "-create": {
                        accountView.printAccountName();
                        String account = reader.readLine();
                        accountDao.create(account, user);
                        log.info("Created account {} for User {}", account, user.getId());
                        break;
                    }
                    case "-exit" : {
                        this.isRunning = false;
                        controllerView.printCommand();
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
