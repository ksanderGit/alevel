package financeApi.controller;

import financeApi.dao.AccountDao;
import financeApi.dao.CategoryDao;
import financeApi.dao.OperationDao;
import financeApi.dao.UserDao;
import financeApi.entities.User;
import financeApi.service.OperationByJDBC;
import financeApi.service.Extractor;
import financeApi.view.ControllerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class AppController {
    private static final Logger log = LoggerFactory.getLogger(AppController.class);
    private final RegistrationController registrationController;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final ControllerView controllerView;
    private final Extractor extractor;
    private User currentUser;
    private Boolean isRunning = true;

    private final Map<String, Consumer<User>> controllers;

    public AppController(UserDao userDao, AccountDao accountDao, CategoryDao categoryDao, OperationDao operationDao, OperationByJDBC operationByJDBC) {
        controllerView = new ControllerView();
        extractor = new Extractor(operationByJDBC);
        registrationController = new RegistrationController(userDao);
        AccountController accountController = new AccountController(accountDao);
        CategoryController categoryController = new CategoryController(categoryDao);
        OperationController operationController = new OperationController(operationDao, categoryDao, accountDao);

        controllers = new HashMap<>();
        controllers.put("-accounts", accountController::loop);
        controllers.put("-categories", categoryController::loop);
        controllers.put("-operations", operationController::loop);
    }

    public void loop() {
        log.info("Application is running");
        controllerView.printLaunch();
        try {
            while (currentUser == null) {
                this.currentUser = registrationController.getUser();
            }
            controllerView.printCommand();
            while (isRunning) {
                String result = reader.readLine();
                if (result.equals("-exit")) {
                    this.isRunning = false;
                    log.info("Application closed");
                }
                if (result.equals("-transfer")) {
                    controllerView.printFrom();
                    String from = reader.readLine();
                    controllerView.printTo();
                    String to = reader.readLine();
                    extractor.toExtract(from, to);
                }
                controllers.getOrDefault(result, user -> controllerView.printCommand()).accept(currentUser);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
