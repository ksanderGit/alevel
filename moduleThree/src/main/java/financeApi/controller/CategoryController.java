package financeApi.controller;

import financeApi.dao.CategoryDao;
import financeApi.entities.User;
import financeApi.view.CategoryView;
import financeApi.view.ControllerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class CategoryController implements Controller {
    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);
    private final CategoryView categoryView;
    private final ControllerView controllerView;
    private final CategoryDao categoryDao;
    private Boolean isRunning = true;

    public CategoryController(CategoryDao catDao) {
        categoryView = new CategoryView();
        controllerView = new ControllerView();
        categoryDao = catDao;
    }
    @Override
    public void loop(User user) {
        log.info("User {} launched account controller", user.getId());
        try {
            while (isRunning) {
                categoryView.printLaunch();
                String result = reader.readLine();
                switch (result) {

                    case "-lists": {
                        log.info("List categories for user {}", user.getId());
                        categoryView.printCategories(user.getCategories());
                        break;
                    }
                    case "-create": {
                        categoryView.printAccount();
                        String res = reader.readLine();
                        categoryView.printName();
                        String name = reader.readLine();
                        categoryView.printSum();
                        long amount = Long.parseLong(reader.readLine());
                        if (amount < 5) {
                            categoryView.printSumError();
                            break;
                        }
                        categoryView.printType();
                        categoryDao.create(name, res, user,
                                reader.readLine().equals("+"), amount);
                        log.info("Created category {} for user {}", name, user.getId());
                        break;
                    }
                    case "-exit": {
                        this.isRunning = false;
                        controllerView.printCommand();
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }
}
