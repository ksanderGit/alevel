package financeApi.controller;

import financeApi.entities.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public interface Controller {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    void loop(User user);
}
