package financeApi.controller;

import financeApi.dao.AccountDao;
import financeApi.dao.CategoryDao;
import financeApi.dao.OperationDao;
import financeApi.entities.Account;
import financeApi.entities.Category;
import financeApi.entities.Operation;
import financeApi.entities.User;
import financeApi.view.OperationView;
import financeApi.view.ControllerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class OperationController implements Controller {
    private static final Logger log = LoggerFactory.getLogger(OperationController.class);
    private final OperationView operationView;
    private final OperationDao operationDao;
    private final CategoryDao categoryDao;
    private final ControllerView controllerView;
    private final AccountDao accountDao;
    private Boolean isRunning = true;

    public OperationController(OperationDao operationDao, CategoryDao categoryDao, AccountDao accountDao) {
        operationView = new OperationView();
        this.operationDao = operationDao;
        this.categoryDao = categoryDao;
        this.accountDao = accountDao;
        controllerView = new ControllerView();
    }

    @Override
    public void loop(User user) {
        log.info("User {} launched operation controller", user.getId());
        try {
            while (isRunning) {
                operationView.printLaunch();
                String opperationType = reader.readLine();
                switch (opperationType) {
                    case "-lists": {
                        log.info("List operations for user {}", user.getId());
                        operationView.printOperations(user.getOperations());
                        break;
                    }
                    case "-create": {
                        operationView.printAccounts(user.getAccountList());
                        Account account = accountDao.getAccount(Long.parseLong(reader.readLine()));
                        operationView.printCategories(user.getCategories());
                        List<Category> categories = new ArrayList<>();
                        while (true) {
                            String category = reader.readLine();
                            if (category.equals("-commit")) {
                                break;
                            }
                            Category categoryAdd = categoryDao.getCategory(Long.parseLong(category));

                            if (categories.size() == 0 || categoryAdd.getProfit().equals(categories.get(0).getProfit())) {
                                categories.add(categoryDao.getCategory(Long.parseLong(category)));
                                operationView.printAddOperation();
                            } else {
                                operationView.printError();
                            }
                        }
                        Operation operation = new Operation();
                        operation.setUser(user);
                        operation.setAccount(account);
                        operationDao.create(operation, categories);
                        log.info("Created operation {} for user {}", operation,
                                user.getId());
                    }
                    case "-exit": {
                        this.isRunning = false;
                        controllerView.printCommand();
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
