package financeApi.controller;

import financeApi.dao.UserDao;
import financeApi.entities.User;
import financeApi.view.RegistrationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.Scanner;

public class RegistrationController {
    private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final RegistrationView registrationView;
    private final UserDao userDao;

    public RegistrationController(UserDao userDao) {
        registrationView = new RegistrationView();
        this.userDao = userDao;
    }

    public User getUser() {
        try {
            while (true) {
                registrationView.printLaunch();
                switch (reader.readLine()) {
                    case "-login": {
                        registrationView.printLogin();
                        String login = reader.readLine();
                        registrationView.printPassword();
                        String password = reader.readLine();
                        Optional<User> admin = userDao
                                .findUserAndPassword(login, password);
                        if (admin.isPresent()) {
                            log.info("User {} logged in", admin.get());
                            registrationView.printRegistered();
                            return admin.get();
                        } else {
                            log.info("User {} not found", login);
                            registrationView.printError(login, password);
                        }
                        break;
                    }
                    case "-reg": {
                        registrationView.printLogin();
                        String login = reader.readLine();
                        registrationView.printPassword();
                        String password = reader.readLine();
                        log.info("Registering user {}", login);
                        userDao.register(login, password);
                        break;
                    }
                }
            }
        }catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
