package financeApi.dao;

import financeApi.entities.Account;
import financeApi.entities.User;
import org.hibernate.Session;

public class AccountDao {
    private final Session session;

    public AccountDao(Session session) {
        this.session = session;
    }

    public void create(String name, User user) {
        Account account = new Account();
        account.setBalance(0L);
        account.setName(name);
        account.setUser(user);
        session.beginTransaction();
        session.save(account);
        session.refresh(user);
        session.getTransaction().commit();
    }

    public Account getAccount(Long id) {
        return session.get(Account.class, id);
    }
}
