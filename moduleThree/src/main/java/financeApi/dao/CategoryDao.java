package financeApi.dao;

import financeApi.entities.Category;
import financeApi.entities.User;
import org.hibernate.Session;

public class CategoryDao {
    private final Session session;

    public CategoryDao(Session session) {
        this.session = session;
    }

    public void create(String name, String accountNumber, User user, Boolean profit, Long amount) {
        Category category = new Category();
        category.setAccountNumber(accountNumber);
        category.setName(name);
        category.setUser(user);
        category.setProfit(profit);
        category.setAmount(amount);

        session.beginTransaction();
        session.save(category);
        session.refresh(user);
        session.getTransaction().commit();

    }

    public Category getCategory(Long id) {
        return session.get(Category.class, id);
    }
}
