package financeApi.dao;

import financeApi.entities.Account;
import financeApi.entities.Category;
import financeApi.entities.Operation;
import org.hibernate.Session;

import java.util.List;

public class OperationDao {
    private final Session session;

    public OperationDao(Session session) {
        this.session = session;
    }

    public void create(Operation operation, List<Category> categories) {
        session.beginTransaction();
        Long sum = 0L;
        for (Category category : categories) {
            if (category.getProfit()) {
                sum += category.getAmount();
            } else {
                sum -= category.getAmount();
            }
        }
        operation.setQuantity(sum);
        Account account = session.get(Account.class, operation.getAccount().getId());
        account.setBalance(sum);
        session.save(operation);
        //session.flush();
        session.getTransaction().commit();
    }
}
