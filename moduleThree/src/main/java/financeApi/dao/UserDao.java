package financeApi.dao;

import financeApi.entities.User;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public class UserDao {
    private final Session session;

    public UserDao(Session session) {
        this.session = session;
    }

    public Optional<User> findUserAndPassword(String username, String password) {
        session.beginTransaction();
        CriteriaBuilder critBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = critBuilder.createQuery(User.class);
        Root<User> from = criteria.from(User.class);
        criteria.select(from);

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        Predicate predicate = criteriaBuilder.and(criteriaBuilder.equal(from.get("name"), username),
                        criteriaBuilder.equal(from.get("password"), password));
        criteria.where(predicate);
        List<User> foundUsers = session.createQuery(criteria).getResultList();
        session.getTransaction().commit();

        if (foundUsers.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(foundUsers.get(0));
    }

    public void register(String username, String password) {
        User user = new User();
        user.setName(username);
        user.setPassword(password);

        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();

    }
}
