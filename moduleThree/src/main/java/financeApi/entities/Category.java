package financeApi.entities;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long amount;
    private String accountNumber;
    private Boolean profit;
    @Embedded
    private TimeStamps timeStamps = TimeStamps.now();
    @ManyToOne
    @JoinColumn(nullable = false, name = "user_id")
    private User user;


    public Boolean getProfit() {
        return profit;
    }

    public void setProfit(Boolean profit) {
        this.profit = profit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", accountNumber='" + accountNumber + '\'' +
                ", profit=" + profit +
                '}';
    }
}
