package financeApi.entities;

import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Instant;
@Embeddable
public class TimeStamps {
    @Column(name = "created_at", nullable = false)
    private Instant createAt;

    @Column(name = "modified_at", nullable = false)
    private Instant modifiedAt;

    public TimeStamps() {
    }

    public TimeStamps(Instant createAt, Instant modifiedAt) {
        this.createAt = createAt;
        this.modifiedAt = modifiedAt;
    }

    public static TimeStamps now(){
        var now = Instant.now();
        return new TimeStamps(now, now);
    }

    public Instant getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Instant createAt) {
        this.createAt = createAt;
    }

    public Instant getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}
