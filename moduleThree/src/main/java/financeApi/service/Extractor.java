package financeApi.service;

import financeApi.controller.CategoryController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Extractor {
    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);
    private final OperationByJDBC operationByJDBC;

    public Extractor(OperationByJDBC operationByJDBC) {
        this.operationByJDBC = operationByJDBC;
    }

    public void toExtract(String from, String to) throws FileNotFoundException {
        log.info("Create data file operation.csv");
        List<String> data = new ArrayList<>();
        for (OperationService op : operationByJDBC.getOperations(from,to)){
            data.add(op.toString());
        }
        File file = new File("operations.csv");
        try (PrintWriter printWriter = new PrintWriter(file)) {
            printWriter.println("account,created,amount");
            data.forEach(printWriter::println);
        }
    }
}
