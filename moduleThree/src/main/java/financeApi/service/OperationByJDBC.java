package financeApi.service;

import financeApi.service.OperationService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OperationByJDBC {
    private static Connection connection;
    private List<OperationService> operations = new ArrayList<>();

    public OperationByJDBC(String url, String user, String password) throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    public List<OperationService> getOperations(String from, String to) {
        String sql = "SELECT * FROM operations LEFT JOIN accounts ON operations.account_id = accounts.id WHERE operations.created_at BETWEEN ? AND ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setTimestamp(1, Timestamp.valueOf(from));
            preparedStatement.setTimestamp(2, Timestamp.valueOf(to));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    operations.add(new OperationService(resultSet.getString("name"),
                            resultSet.getString("created_at"), resultSet.getInt("quantity")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return operations;
    }
}
