package financeApi.service;

public class OperationService {
    private String account;
    private String created;
    private Integer amount;

    public OperationService(String account, String created, Integer amount) {
        this.account = account;
        this.created = created;
        this.amount = amount;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "account='" + account + '\'' +
                ", created='" + created + '\'' +
                ", amount=" + amount;
    }
}
