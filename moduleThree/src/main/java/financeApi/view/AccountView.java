package financeApi.view;

import financeApi.entities.Account;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class AccountView implements View{
    private static final String LAUNCH_MESSAGE = "Enter -lists to list accounts, -create to create account, -exit \n";
    private static final String NAME_ACCOUNT ="Enter name account \n";

    @Override
    public void printLaunch() {
        try {
            outputStream.write(LAUNCH_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printAccountName() {
        try {
            outputStream.write(NAME_ACCOUNT.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printAccounts(List<Account> accounts) {
        for (Account account : accounts) {
            try {
                outputStream.write(account.toString().getBytes());
                outputStream.write(System.lineSeparator().getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
