package financeApi.view;

import financeApi.entities.Category;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class CategoryView implements View {
    private static final String LAUNCH_MESSAGE =
            "Enter -lists to list categories, -create to create category, -exit \n";
    private static final String CATEGORY_MESSAGE =
            "What category is it income or outcome? (+/-) + - income, - - outcome \n";
    private static final String ACCOUNT_MESSAGE = "Account number of an another account \n";
    private static final String CATEGORY_NAME_MESSAGE = "Enter name for category \n";
    private static final String SUM_MESSAGE = "Enter sum \n";
    private static final String SUM_ERROR = "Sum can't be less 5 \n";

    @Override
    public void printLaunch() {
        try {
            outputStream.write(LAUNCH_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printName() {
        try {
            outputStream.write(CATEGORY_NAME_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printAccount() {
        try {
            outputStream.write(ACCOUNT_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printType() {
        try {
            outputStream.write(CATEGORY_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printSum() {
        try {
            outputStream.write(SUM_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printSumError() {
        try {
            outputStream.write(SUM_ERROR.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printCategories(List<Category> categories) {
        for (Category account : categories) {
            try {
                outputStream.write(account.toString().getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
