package financeApi.view;

import java.io.IOException;

public class ControllerView implements View{
    private static final String RUN_MESSAGE = "Finance application is running \n";
    private static final String COMMAND_MESSAGE =
            "COMMANDS: -accounts - control accounts, -operations - control operations, -categories - control categories, -transfer - save data to file \n";
    private static final String FROM =
            "Select date. Date format yyyy-mm-dd hh:mm:ss \n";
    private static final String TO =
            "Select date. Date format yyyy-mm-dd hh:mm:ss \n";

    @Override
    public void printLaunch() {
        try {
            outputStream.write(RUN_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printCommand() {
        try {
            outputStream.write(COMMAND_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printFrom() {
        try {
            outputStream.write(FROM.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printTo() {
        try {
            outputStream.write(TO.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
