package financeApi.view;

import financeApi.entities.Account;
import financeApi.entities.Category;
import financeApi.entities.Operation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class OperationView implements View{
    private static final String LAUNCH_MESSAGE =
            "Enter -lists to list operations, -create to create operation, -exit \n";
    private static final String ACCOUNT_MESSAGE = "Select account \n";
    private static final String CATEGORY_MESSAGE = "Select category \n";
    private static final String CATEGORY_ERROR_MESSAGE = "Impossible add category of different type to this operation \n";
    private static final String OPPERATION_ADD = "Operation added \n";

    @Override
    public void printLaunch() {
        try {
            outputStream.write(LAUNCH_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printError() {
        try {
            outputStream.write(CATEGORY_ERROR_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printAccounts(List<Account> accounts) {
        try {
            outputStream.write(ACCOUNT_MESSAGE.getBytes());
            for (Account account : accounts) {
                outputStream.write((account.getId() + " " + account.toString() + "\n").getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printCategories(List<Category> categories) {
        try {
            outputStream.write(CATEGORY_MESSAGE.getBytes());
            for (Category category : categories) {
                outputStream.write((category.getId() + " " + category.toString() + "\n").getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printOperations(List<Operation> operations) {
        for (Operation account : operations) {
            try {
                outputStream.write((account.toString() + "\n").getBytes());

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    public void printAddOperation() {
        try {
            outputStream.write(OPPERATION_ADD.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
