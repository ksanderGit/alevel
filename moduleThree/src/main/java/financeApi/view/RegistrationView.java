package financeApi.view;

import java.io.IOException;

public class RegistrationView implements View {
    private static final String LAUNCH_MESSAGE = "Enter -login to enter or enter -reg to registration \n";

    private static final String LOGIN_MESSAGE = "Enter your login \n";

    private static final String PASSWORD_MESSAGE = "Enter your password \n";

    private static final String ERROR_MESSAGE = "Not found user login %s%n password %s \n";

    private static final String REGISTER_MESSAGE = "Registered \n";


    @Override
    public void printLaunch() {
        try {
            outputStream.write(LAUNCH_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printLogin() {
        try {
            outputStream.write(LOGIN_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printPassword() {
        try {
            outputStream.write(PASSWORD_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printError(String username, String password) {
        try {
            outputStream.write(String.format(ERROR_MESSAGE, username, password).getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void printRegistered() {
        try {
            outputStream.write(REGISTER_MESSAGE.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
