package financeApi.view;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public interface View {
    OutputStream outputStream = new PrintStream(System.out);
    void printLaunch();
}
