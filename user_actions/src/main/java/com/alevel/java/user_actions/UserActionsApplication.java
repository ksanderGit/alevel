package com.alevel.java.user_actions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserActionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserActionsApplication.class, args);
    }

}
