package com.alevel.java.user_actions.controller;

import com.alevel.java.user_actions.entity.TodoContent;
import com.alevel.java.user_actions.exceptions.ContentNotFoundException;
import com.alevel.java.user_actions.exceptions.MessageNotFoundException;
import com.alevel.java.user_actions.service.TodoActions;
import com.alevel.java.user_actions.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/todocontent")
public class TodoController {
    private final TodoActions todoActions;

    public TodoController(TodoActions todoActions) {
        this.todoActions = todoActions;
    }

    @GetMapping("/{id}")
    public TodoContent getById(@PathVariable Long id) {
        return todoActions.getById(id).orElseThrow(ContentNotFoundException::new);
    }

    @GetMapping
    public List<TodoContent> getAllNotDone() {
        return todoActions.getAllNotDone();
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public TodoContent create(@RequestBody TodoContent todoContent) {
        return todoActions.create(todoContent);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void updateItem(@PathVariable Long id, @RequestBody TodoContent todoContent) {
        todoActions.update(id, todoContent);
    }
    @DeleteMapping("/{id}")
    public TodoContent deleteItem(@PathVariable Long id) {
        return todoActions.deleteById(id).orElseThrow(() -> new ContentNotFoundException(id));
    }

}
