package com.alevel.java.user_actions.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ContentNotFoundException extends ResponseStatusException {
    public ContentNotFoundException() {
        super(HttpStatus.NO_CONTENT);
    }

    public ContentNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, "Content not found " + id);
    }
}
