package com.alevel.java.user_actions.repository;

import com.alevel.java.user_actions.entity.TodoContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepository extends JpaRepository<TodoContent, Long> {
    List<TodoContent> findByDoneFalse();
}
