package com.alevel.java.user_actions.service;

import com.alevel.java.user_actions.entity.TodoContent;

import java.util.List;
import java.util.Optional;

public interface TodoActions {
    Optional<TodoContent> getById(Long id);

    List<TodoContent> getAllNotDone();

    TodoContent create(TodoContent todoContent);

    void updateAll(Iterable<TodoContent> todoContents);

    void  update(Long id, TodoContent request);

    Optional<TodoContent> deleteById(Long id);
}
