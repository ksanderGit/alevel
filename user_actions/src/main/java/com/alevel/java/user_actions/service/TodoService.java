package com.alevel.java.user_actions.service;

import com.alevel.java.user_actions.entity.TodoContent;
import com.alevel.java.user_actions.exceptions.ContentNotFoundException;
import com.alevel.java.user_actions.repository.TodoRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
@Service
@Transactional
public class TodoService implements TodoActions {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<TodoContent> getAllNotDone() {
        return todoRepository.findByDoneFalse();
    }

    @Override
    public TodoContent create(TodoContent todoContent) {
        todoRepository.save(todoContent);
        return todoContent;
    }

    @Override
    public void updateAll(Iterable<TodoContent> todoContents) {
        todoRepository.saveAll(todoContents);
    }

    @Override
    public Optional<TodoContent> getById(Long id) {
        return todoRepository.findById(id);
    }

    @Override
    public Optional<TodoContent> deleteById(Long id) {
        Optional<TodoContent> todoContent = todoRepository.findById(id);
        todoContent.ifPresent(todoRepository::delete);
        return todoContent;
    }

    @Override
    public void update(Long id, TodoContent request) {
        TodoContent todoContent = todoRepository.findById(id).orElseThrow(ContentNotFoundException::new);
            todoContent.setText(request.getText());
            todoContent.setDone(request.getDone());
            todoRepository.save(todoContent);
    }
}
