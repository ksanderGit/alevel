import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


//@WebServlet(name = "Servlet", urlPatterns = "/servlet")
public class Servlet extends HttpServlet {
    private final ConcurrentHashMap<String, String> addresses = new ConcurrentHashMap<>();
    private StringBuilder stringBuilder = new StringBuilder();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        String host = req.getRemoteHost();
        String userAgent = req.getHeader("User-Agent");
        addresses.put(host, userAgent);
        for (Map.Entry entry : addresses.entrySet()) {
            if (entry.getKey().equals(host)) {
                stringBuilder.append("<b> " + entry.getKey() + " :: " + entry.getValue() + "</b>");
            }else {
                stringBuilder.append(entry.getKey() + " :: " + entry.getValue());
            }
        }
       resp.getWriter().print(stringBuilder.toString());
    }
}
